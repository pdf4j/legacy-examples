This is a copy of the Utopia Type-1 fonts which Adobe contributed to the
X consortium, renamed for use with TeX:
  
  Utopia-Regular
  Utopia-Italic
  Utopia-Bold
  Utopia-BoldItalic

  Permission to use, reproduce, display and distribute the listed
  typefaces is hereby granted, provided that the Adobe Copyright notice
  appears in all whole and partial copies of the software and that the
  following trademark symbol and attribution appear in all unmodified
  copies of the software:

        Copyright (c) 1989 Adobe Systems Incorporated
        Utopia (R)
        Utopia is a registered trademark of Adobe Systems Incorporated

TeX metrics, virtual fonts and LaTeX font definition files can be found
in the archive CTAN:fonts/metrics/freenfss.zip.

==============================================================================

Computer Modern PostScript Fonts
(Adobe Type 1 format)
2001/06/06

-----------------------------------------------------------------------------
The PostScript Type 1 implementation of the Computer Modern fonts produced by
and previously distributed by Blue Sky Research and Y&Y, Inc. are now freely
available for general use. This has been accomplished through the cooperation
of a consortium of scientific publishers with Blue Sky Research and Y&Y.
Members of this consortium include:

        Elsevier Science
        IBM Corporation
        Society for Industrial and Applied Mathematics (SIAM)
        Springer-Verlag
        American Mathematical Society (AMS)

In order to assure the authenticity of these fonts, copyright will be held
by the American Mathematical Society. This is not meant to restrict in any
way the legitimate use of the fonts, such as (but not limited to) electronic
distribution of documents containing these fonts, inclusion of these fonts
into other public domain or commercial font collections or computer
applications, use of the outline data to create derivative fonts and/or
faces, etc. However, the AMS does require that the AMS copyright notice be
removed from any derivative versions of the fonts which have been altered in
any way. In addition, to ensure the fidelity of TeX documents using Computer
Modern fonts, Professor Donald Knuth, creator of the Computer Modern faces,
has requested that any alterations which yield different font metrics be
given a different name.

The AMS does not provide technical support or installation assistance
beyond any installation instructions included in this file.  Installation
and use of these fonts may require some technical expertise.  Review this
README file in its entirety before undertaking an installation.

The fonts are available in Macintosh and PFB (binary Type 1) outline
formats. Users requiring the fonts in PFA form should convert them with the
aid of one of the following tools, available from CTAN:

         -   fonts/utilities/ps2mf/pfb2pfa
         -   fonts/utilities/ps2pk/ps2pk15/misc/pfb2pfa
         -   systems/msdos/4alltex/diskp1/pfb2pfa.zip

The canonical version of the Computer Modern PostScript Fonts is located on
the AMS server which can be reached either by FTP or via the Web:
    ftp ftp.ams.org  in  /pub/tex/psfonts/cm
    http://www.ams.org/tex/type1-fonts.html
These fonts are also available on the Comprehensive TeX Archive Network (CTAN)
in fonts/cm/ps-type1/bluesky.

This distribution does not contain the TFM files that are necessary to use
the fonts with TeX; the TFM files can be obtained from CTAN, in fonts/cm/tfm.

The file "cmsample.tex" which is included in this distribution includes
samples of all of the fonts in the collection. Once you have installed the
fonts, you may typeset this file in Plain TeX to test your font installation.

----------------------------------------------------------------------------
Installation Instructions for Windows

Please make sure that you have a fairly recent version of ATM (at
least version 2.5).

If you are not using font management software:

To install the outline fonts for use in Windows, open the ATM control
panel and select "Add".  Choose the fonts from the "pfmfiles" subdirectory
of the "Fonts" directory in this distribution which you wish to install.
You can select several fonts at once by holding the control key while
clicking on font names, or you can hold the mouse button down and drag the
cursor to select arange of fonts.  After selecting the fonts, click "Add".

If you are using font management software, please follow follow the instructions
that came with the software to install the fonts.

----------------------------------------------------------------------------
Using the CM fonts in Adobe Type 1 format with TeX:

The CM fonts in Adobe Type 1 format have exactly the same metrics as
corresponding bitmapped CM fonts, hence the same TFM files are used by TeX.
TeX concerns itself only with metric information --- which is contained
entirely in the TFM file. The same DVI file is used for CM fonts in Adobe
Type 1 format as for corresponding bitmapped CM fonts in PK format.

The DVI processor (previewer or printer driver), however, has to be able to
deal with fonts in Adobe Type 1 format (referred to as `PostScript' fonts by
some).  How this works is a function of which DVI processor is being used.

Read the manual for your DVI processor(s), for example XDVI, dvips, DVIPSONE,
DVIWindo.
==============================================================================

Copyright (c) 1986-2002 Kim Jeong-Hwan
All rights reserved.

Permission to use, copy, modify and distribute this font is
hereby granted, provided that both the copyright notice and
this permission notice appear in all copies of the font,
derivative works or modified versions, and that the following
acknowledgement appear in supporting documentation:
    Baekmuk Batang, Baekmuk Dotum, Baekmuk Gulim, and
    Baekmuk Headline are registered trademarks owned by
    Kim Jeong-Hwan.
==============================================================================
The shavian fonts were downloaded from the website of Ethan Lamoreaux
http://www.30below.com/~ethanl/fonts.html

Created by Ethan Lamoreaux with PfaEdit 1.0 (http://pfaedit.sf.net)
Copyright (C) 2003 Ethan S. Lamoreaux
Ethan writes inside the font: "Freeware.
But I'm not to be held responsible for any problems which may arise due
to the use of this font.  Use it at your own risk. If you wish to use
this in a commercial product, please contact me first."

On his site, we read: "These fonts are made available to the public
for almost any purpose. I only ask that if you want to use them for
commercial purposes, please contact me first. Also, if you find a good
use for these fonts, it would be nice if you could drop me an email
and let me know how well they work for you."

I (Bruno) have mailed Ethan on November 22, 2005