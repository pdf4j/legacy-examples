INSERT INTO film_category (category_id, name, keyword) VALUES (0, 'Unknown', '');
INSERT INTO film_category (category_id, name, keyword) VALUES (1, 'Official selection (in competition)', 'comp');
INSERT INTO film_category (category_id, name, keyword) VALUES (2, 'Official selection (preview)', 'prev');
INSERT INTO film_category (category_id, name, keyword) VALUES (3, 'World cinema', 'world');
INSERT INTO film_category (category_id, name, keyword) VALUES (4, 'History of film', 'hist');
INSERT INTO film_category (category_id, name, keyword) VALUES (5, 'A Look Apart', 'apart');
INSERT INTO film_category (category_id, name, keyword) VALUES (6, 'Focus', 'focus');