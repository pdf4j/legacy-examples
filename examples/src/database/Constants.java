/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package database;

public class Constants {

	public static final String SCRIPTS = "resources/sql/";
	public static final String DRIVER = "org.hsqldb.jdbcDriver";
	public static final String CONNECTSTRING = "jdbc:hsqldb:file:resources/hsqldb/film";
	public static final String USERNAME = "sa";
	public static final String PASSWORD = "";
}
