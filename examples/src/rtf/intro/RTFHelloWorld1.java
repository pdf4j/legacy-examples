package rtf.intro;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.rtf.RtfWriter2;
/**
/*
 * This example was written by Howard Shank, contributor for iText.
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 * 
 * @author Howard Shank (hgshank@yahoo.com)
 * @since 8/11/2008
 */
public class RTFHelloWorld1 {

	static String outFile = "RtfHelloWorld1.rtf";
	static String  resultsPath = "results/rtf/intro/";
	static String  resourcePath = "resources/rtf/intro/";
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// create a document
		Document document = new Document();

		try {
			// create a RTF Writer
			RtfWriter2.getInstance(document, new FileOutputStream(resultsPath + outFile));
			
			// Open the document for processing
			document.open();
			
			// add some text
			document.add(new Paragraph("Hello World"));
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		
		// close the document
		document.close();
	}

}
