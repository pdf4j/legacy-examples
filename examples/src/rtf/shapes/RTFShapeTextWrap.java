package rtf.shapes;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.rtf.RtfWriter2;
import com.lowagie.text.rtf.graphic.RtfShape;
import com.lowagie.text.rtf.graphic.RtfShapePosition;
import com.lowagie.text.rtf.graphic.RtfShapeProperty;

/**
 * This example was written by Howard Shank, contributor for iText.
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 * 
 * NOTE: This example requires iText 2.1.4 or greater to work.
 * 
 * @author Howard Shank (hgshank@yahoo.com)
 * @since 8/27/2008
 */
public class RTFShapeTextWrap {

	static String outFile = "RtfShapeTextWrap.rtf";
	static String inFile1 = "logo.png";
	static String resultsPath = "results/rtf/shapes/";
	static String resourcePath = "resources/rtf/shapes/";

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Document document = new Document();

		try {
			RtfWriter2.getInstance(document,
					new FileOutputStream(resultsPath + outFile));
			document.open();
			Image logo = Image.getInstance(resourcePath + inFile1);

			Paragraph p = new Paragraph();
			Chunk c = new Chunk();
			c
					.append("This is some text. This is some text. This is some text. This is some text. This is some text. This is some text. "
							+ "This is some text. This is some text. This is some text. This is some text. This is some text. This is some text. "
							+ "This is some text. This is some text. This is some text. This is some text. This is some text. This is some text. "
							+ "This is some text. This is some text. This is some text. This is some text. This is some text. This is some text. "
							+ "This is some text. This is some text. This is some text. This is some text. This is some text. This is some text. "
							+ "This is some text. This is some text. This is some text. This is some text. This is some text. This is some text. "
							+ "This is some text. This is some text. This is some text. This is some text. This is some text. This is some text. "
							+ "This is some text. This is some text. This is some text. This is some text. This is some text. This is some text. "
							+ "This is some text. This is some text. This is some text. This is some text. This is some text. This is some text. "
							+ "This is some text. This is some text. This is some text. This is some text. This is some text. This is some text. ");

			RtfShapePosition position = new RtfShapePosition(2000, 6000, 10500,
					4500);
			RtfShape shape = new RtfShape(RtfShape.SHAPE_RECTANGLE, position);
			shape.setWrapping(RtfShape.SHAPE_WRAP_LEFT);
			shape.setProperty(new RtfShapeProperty(
					RtfShapeProperty.PROPERTY_IMAGE, logo));

			p.add(shape);
			p.add(c);
			document.add(p);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (BadElementException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}
}
