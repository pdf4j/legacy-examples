/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.directcontent;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;

public class Logo {

	public static final String RESULT = "results/questions/directcontent/logo.pdf";
	
	public static void main(String[] args) {
		Rectangle rect = new Rectangle(36, 726, 136, 786);
		Document document = new Document(rect);
		try {
			PdfWriter writer = 
				PdfWriter.getInstance(document, new FileOutputStream(RESULT));
			document.open();
			PdfContentByte canvas = writer.getDirectContent();
			canvas.moveTo(63.73f, 735.47f);
			canvas.curveTo(72.13f, 737.94f, 75.73f, 758.15f, 79.67f, 768.04f);
			canvas.curveTo(84.82f, 767.79f, 94.41f, 772.38f, 85.69f, 772.97f);
			canvas.curveTo(83.19f, 768.56f, 63.84f, 779.61f, 60.55f, 779.7f);
			canvas.curveTo(48.58f, 780.03f, 74.62f, 769.75f, 74.68f, 769.99f);
			canvas.curveTo(74.64f, 769.8f, 69.43f, 743.55f, 66.08f, 740.12f);
			canvas.curveTo(59.24f, 733.11f, 52.73f, 745.57f, 53.35f, 752.21f);
			canvas.curveTo(53.6f, 754.85f, 64.32f, 747.14f, 57.21f, 755.02f);
			canvas.curveTo(45.66f, 767.82f, 49.11f, 731.17f, 63.73f, 735.47f);
			canvas.moveTo(93.61f, 741.57f);
			canvas.curveTo(109.15f, 721.02f, 112.42f, 746.51f, 118.3f, 761.57f);
			canvas.curveTo(124.73f, 761.48f, 129.8f, 765.3f, 124f, 765.62f);
			canvas.curveTo(119.84f, 761.88f, 108.12f, 770.22f, 100.92f, 771.57f);
			canvas.curveTo(94.24f, 769.12f, 115.02f, 764.34f, 114.24f, 761.53f);
			canvas.curveTo(109.9f, 751.94f, 107.5f, 721.65f, 95.84f, 744.74f);
			canvas.curveTo(100.15f, 747.38f, 109.63f, 766.92f, 95.28f, 748.57f);
			canvas.curveTo(91.45f, 743.68f, 89.6f, 766.35f, 77.99f, 754.83f);
			canvas.curveTo(74.22f, 751.07f, 84.27f, 747.81f, 81.6f, 745.28f);
			canvas.curveTo(67.39f, 750.93f, 74.66f, 719.55f, 93.61f, 741.57f);
			canvas.moveTo(78.37f, 743.72f);
			canvas.curveTo(78.19f, 743.22f, 89.27f, 744.04f, 82.46f, 749.7f);
			canvas.curveTo(76.28f, 754.83f, 85.17f, 760.2f, 90.72f, 746.97f);
			canvas.curveTo(94f, 739.15f, 73.88f, 730.88f, 78.37f, 743.72f);
			canvas.setCMYKColorFillF(1, 0.5f, 0, 0.467f);
			canvas.fill();
			canvas.setCMYKColorFillF(0, 0.467f, 1, 0);
			canvas.moveTo(58f, 764.8f);
			canvas.curveTo(58f, 767.01f, 56.21f, 768.8f, 54f, 768.8f);
			canvas.curveTo(51.79f, 768.8f, 50f, 767.01f, 50f, 764.8f);
			canvas.curveTo(50f, 762.59f, 51.79f, 760.8f, 54f, 760.8f);
			canvas.curveTo(56.21f, 760.8f, 58f, 762.59f, 58f, 764.8f);
			canvas.fill();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}

}
