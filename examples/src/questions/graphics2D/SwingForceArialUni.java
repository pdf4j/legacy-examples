/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.graphics2D;

import java.awt.Font;
import java.awt.Graphics2D;
import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.FontMapper;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;

public class SwingForceArialUni {
	
	public static final String RESULT = "results/questions/graphics2d/swing_arialuni.pdf";
	
	public static void main(String[] args) {
		Document document = new Document(new Rectangle(210, 25));
		try {
			PdfWriter writer = PdfWriter.getInstance(
					document, new FileOutputStream(RESULT));
			document.open();
			PdfContentByte cb = writer.getDirectContent();
			FontMapper arialuni = new FontMapper() {
				public BaseFont awtToPdf(Font font) {
					try {
						return BaseFont.createFont(
								"c:/windows/fonts/arialuni.ttf",
								BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
					} catch (DocumentException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					return null;
				}

				public Font pdfToAwt(BaseFont font, int size) {
					return null;
				}
				
			};
			Graphics2D g2 = cb.createGraphics(200, 50, arialuni);
			g2.setFont(null);
			g2.drawString("Greek mu: \u03bc - \u039c; degree symbol: \u00b0", 0, 40);
			g2.dispose();
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
 
		// step 5: we close the document
		document.close();
	}
}
