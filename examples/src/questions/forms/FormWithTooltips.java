/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.forms;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.ExceptionConverter;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfAnnotation;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.TextField;

public class FormWithTooltips implements PdfPCellEvent {

	public static final String RESULT = "results/questions/forms/form_with_tooltips.pdf";
	
	protected PdfFormField parent;

	protected PdfFormField kid;

	protected float padding;
	
	protected String tooltip;
	
	FormWithTooltips(PdfFormField parent, PdfFormField kid, float padding, String tooltip) {
		this.parent = parent;
		this.kid = kid;
		this.padding = padding;
		this.tooltip = tooltip;
	}

	/**
	 * @see com.lowagie.text.pdf.PdfPCellEvent#cellLayout(com.lowagie.text.pdf.PdfPCell,
	 *      com.lowagie.text.Rectangle, com.lowagie.text.pdf.PdfContentByte[])
	 */
	public void cellLayout(PdfPCell cell, Rectangle rect, PdfContentByte[] cb) {
		kid.setWidget(new Rectangle(rect.getLeft(padding), rect.getBottom(padding),
				rect.getRight(padding), rect.getTop(padding)),
				PdfAnnotation.HIGHLIGHT_INVERT);
		kid.setUserName(tooltip);
		try {
			parent.addKid(kid);
		} catch (Exception e) {
			throw new ExceptionConverter(e);
		}
	}
	
	private static PdfPTable createTable(PdfWriter writer, PdfFormField parent)
			throws IOException, DocumentException {
		PdfPTable table = new PdfPTable(2);
		PdfPCell cell;
		TextField field;
		table.getDefaultCell().setPadding(5f);

		table.addCell("Your name:");
		cell = new PdfPCell();
		field = new TextField(writer, new Rectangle(0, 0), "name");
		field.setFontSize(12);
		cell.setCellEvent(new FormWithTooltips(parent, field.getTextField(), 1, "Your name"));
		table.addCell(cell);

		table.addCell("Your home address:");
		cell = new PdfPCell();
		field = new TextField(writer, new Rectangle(0, 0), "address");
		field.setFontSize(12);
		cell.setCellEvent(new FormWithTooltips(parent, field.getTextField(), 1, "Street and number"));
		table.addCell(cell);

		table.addCell("Postal code:");
		cell = new PdfPCell();
		field = new TextField(writer, new Rectangle(0, 0), "postal_code");
		field.setFontSize(12);
		cell.setCellEvent(new FormWithTooltips(parent, field.getTextField(), 1, "Postal code"));
		table.addCell(cell);

		table.addCell("City:");
		cell = new PdfPCell();
		field = new TextField(writer, new Rectangle(0, 0), "city");
		field.setFontSize(12);
		cell.setCellEvent(new FormWithTooltips(parent, field.getTextField(), 1, "City"));
		table.addCell(cell);

		table.addCell("Country:");
		cell = new PdfPCell();
		field = new TextField(writer, new Rectangle(0, 0), "country");
		field.setFontSize(12);
		cell.setCellEvent(new FormWithTooltips(parent, field.getTextField(), 1, "Country"));
		table.addCell(cell);

		table.addCell("Your email address:");
		cell = new PdfPCell();
		field = new TextField(writer, new Rectangle(0, 0), "email");
		field.setFontSize(12);
		cell.setCellEvent(new FormWithTooltips(parent, field.getTextField(), 1, "mail address"));
		table.addCell(cell);
		return table;
	}
	

	public static void main(String[] args) {
		Document document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document,
					new FileOutputStream(RESULT));
			document.open();
			PdfFormField person = PdfFormField.createEmpty(writer);
			person.setFieldName("person");
			document.add(createTable(writer, person));
			writer.addAnnotation(person);
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		// step 5
		document.close();
	}
}
