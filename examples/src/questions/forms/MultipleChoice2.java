/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.forms;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class MultipleChoice2 extends MultipleChoice {

	public static final String RESULT2 = "results/questions/forms/multiplechoice2.pdf";
	
	public static void main(final String[] args)
			throws IOException, DocumentException {
		createPdf();
		PdfReader reader = new PdfReader(RESULT);
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(RESULT2));
		AcroFields form = stamper.getAcroFields();
		form.setListSelection("iText", new String[]{"C", "PHP"});
		stamper.close();
	}
}
