/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.forms;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class ChangeTextFieldAlignment {

	public static final String RESOURCE = "resources/questions/forms/hello_who.pdf";
	public static final String RESULT = "results/questions/forms/hello_alignment.pdf";
	
	public static void main(String[] args) {
		try {
			PdfReader reader = new PdfReader(RESOURCE);
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(RESULT));
			AcroFields form = stamper.getAcroFields();
			PdfDictionary dict = form.getFieldItem("Who").getMerged(0);
			dict.put(PdfName.Q, new PdfNumber(1));
			form.setField("Who", "Center of the World");
			stamper.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
