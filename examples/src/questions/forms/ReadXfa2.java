/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */
package questions.forms;

import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.XfaForm;

public class ReadXfa2 {

	public static final String RESOURCE = "resources/questions/forms/designer_form.pdf";
	public static final String RESULT = "results/questions/forms/xfa2.xml";

	public static void main(String[] args) {
		try {
			PdfReader reader = new PdfReader(RESOURCE);
			FileOutputStream os = new FileOutputStream(RESULT);
			XfaForm xfa = new XfaForm(reader);      	
        	Document doc = xfa.getDomDocument();   	
        	Transformer tf = TransformerFactory.newInstance().newTransformer();
        	tf.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        	tf.setOutputProperty(OutputKeys.INDENT, "yes");
        	tf.transform(new DOMSource(doc), new StreamResult(os));
        	reader.close();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (TransformerException e) {
			e.printStackTrace();
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}
}
