/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.forms;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Map;
import java.util.Set;

import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfLister;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.AcroFields.Item;

public class GetTextFields {

	public static final String RESOURCE = "resources/questions/forms/textfields.pdf";
	public static final String RESULT = "results/questions/forms/textfieldinfo.txt";
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		try {
			PrintStream out = new PrintStream(new File(RESULT));
			
			PdfLister lister = new PdfLister(out);
			
			PdfReader reader = new PdfReader(RESOURCE);
			
			PdfDictionary root = reader.getCatalog();
			PdfDictionary acroform = root.getAsDict(PdfName.ACROFORM);

			out.println("These are the form's font dictionaries:");
			PdfDictionary fonts = acroform.getAsDict(PdfName.DR).getAsDict(PdfName.FONT);
			for (PdfName key : (Set<PdfName>) fonts.getKeys()) {
				lister.listDict((PdfDictionary)PdfReader.getPdfObject(fonts.get(key)));
			}
			out.println("--------------");
			
			out.println("This is the default appearance for the complete form:");
			lister.listAnyObject(PdfReader.getPdfObject(acroform.get(PdfName.DA)));
			out.println("--------------");
			
			AcroFields form = reader.getAcroFields();
			Map<String,Item> fields = form.getFields();
			Item item;
			for (String name : fields.keySet()) {
				out.println(name);
				if (form.getFieldType(name) == AcroFields.FIELD_TYPE_TEXT) {
					item = form.getFieldItem(name);
					PdfDictionary dict = (PdfDictionary)item.getMerged(0);

					out.println("This is the field's font dictionary:");
					fonts = dict.getAsDict(PdfName.DR).getAsDict(PdfName.FONT);
					for (PdfName key : (Set<PdfName>) fonts.getKeys()) {
						lister.listDict((PdfDictionary)PdfReader.getPdfObject(fonts.get(key)));
					}
					out.println("---");
					out.println("This is the field's default appearance");
					lister.listAnyObject(PdfReader.getPdfObject(dict.get(PdfName.DA)));
				}
				else {
					out.println("NOT A TEXT FIELD!");
				}
				out.println("--------------");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
