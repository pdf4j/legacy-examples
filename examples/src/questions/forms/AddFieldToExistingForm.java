/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.forms;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.TextField;

public class AddFieldToExistingForm {

	public static final String FORM = "resources/questions/forms/hello_who.pdf";
	public static final String RESULT = "results/questions/forms/added_field.pdf";
	
	public static void main(String[] args) {
		PdfReader reader;
		try {
			reader = new PdfReader(FORM);
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(RESULT));
			TextField tf = new TextField(stamper.getWriter(),
					new Rectangle(100, 760, 400, 785), "added_field");
			tf.setText("\u00e4\u00f4\u00df");
			tf.setOptions(TextField.READ_ONLY);
			stamper.addAnnotation(tf.getTextField(), 1);
			AcroFields form = stamper.getAcroFields();
			form.setField("Who", "\u00e4\u00f3\u00df\u00f4");
			stamper.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
