/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.forms;

import java.io.FileOutputStream;
import java.io.IOException;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfAnnotation;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPCellEvent;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.TextField;
import com.lowagie.text.pdf.PdfStamper;

public class KidsOnDifferentPages implements PdfPCellEvent {

	public static final String FORM = "results/questions/forms/kids_different_pages.pdf";
	public static final String RESULT = "results/questions/forms/kids_different_pages_filled.pdf";
	
	protected PdfFormField parent;
	protected PdfFormField kid;
	protected float padding;
	protected int pageNo;

	public KidsOnDifferentPages(PdfFormField kid) {
		this.kid = kid;
	}

	public void cellLayout(PdfPCell cell, Rectangle rect, PdfContentByte[] cb) {
		kid.setWidget(new Rectangle(rect.getLeft(padding), rect
				.getBottom(padding), rect.getRight(padding), rect
				.getTop(padding)), PdfAnnotation.HIGHLIGHT_INVERT);
	}

	public static void main(String[] args) {
		createPdf();
		fillPdf();
	}

	public static void createPdf() {
		Document document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document,
					new FileOutputStream(FORM));
			document.open();
			
			// create the parent field and its kids
			PdfFormField person = PdfFormField.createEmpty(writer);
			person.setFieldName("person");
			// one kid on page 1
			TextField field1 = new TextField(writer, new Rectangle(0, 0), "name1");
			PdfFormField kid1 = field1.getTextField();
			kid1.setPlaceInPage(1);
			person.addKid(kid1);
			// another kid on page 2
			TextField field2 = new TextField(writer, new Rectangle(0, 0), "name2");
			PdfFormField kid2 = field2.getTextField();
			kid2.setPlaceInPage(2);
			person.addKid(kid2);
			writer.addAnnotation(person);
			
			// now add the page content
			document.add(createTable(kid1));
			document.newPage();
			document.add(createTable(kid2));
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		document.close();
	}

	private static PdfPTable createTable(PdfFormField kid) throws IOException,
			DocumentException {
		PdfPTable table = new PdfPTable(2);
		table.addCell("Your name:");
		PdfPCell cell = new PdfPCell();
		cell.setCellEvent(new KidsOnDifferentPages(kid));
		table.addCell(cell);
		return table;
	}
	
	public static void fillPdf() {
		try {
			PdfReader reader;
			PdfStamper stamper;
			reader = new PdfReader(FORM);
			stamper = new PdfStamper(reader, new FileOutputStream(RESULT));
			AcroFields form = stamper.getAcroFields();
			form.setField("person.name1", "hello");
			form.setField("person.name2", "world");
			stamper.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}