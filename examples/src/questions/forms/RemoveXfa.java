/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.forms;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Map;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.AcroFields.Item;

public class RemoveXfa {

	public static final String RESOURCE = "resources/questions/forms/designer_form.pdf";
	public static final String RESULT = "results/questions/forms/without_xfa.pdf";
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		try {
			PdfReader reader = new PdfReader(RESOURCE);
			PdfDictionary root = reader.getCatalog();
			PdfDictionary acroform = root.getAsDict(PdfName.ACROFORM);
			acroform.remove(PdfName.XFA);
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(RESULT));
			AcroFields form = stamper.getAcroFields();
			Map<String,Item> fields = form.getFields();
			for (String field : fields.keySet()) {
				System.out.println(field);
				form.setField(field, "value");
			}
			stamper.partialFormFlattening("topmostSubform[0].Page1[0].SN_NUMBER[0]");
			stamper.setFormFlattening(true);
			stamper.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
