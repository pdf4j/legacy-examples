/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.forms;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.pdf.PRStream;
import com.lowagie.text.pdf.PdfArray;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfReader;

public class ReadXfa {

	public static final String RESOURCE = "resources/questions/forms/designer_form.pdf";
	public static final String RESULT = "results/questions/forms/xfa.xml";
	
	public static void main(String[] args) {
		try {
			PdfReader reader = new PdfReader(RESOURCE);
			FileOutputStream os = new FileOutputStream(RESULT);
			PdfDictionary root = reader.getCatalog();
			PdfDictionary acroform = root.getAsDict(PdfName.ACROFORM);
			PdfArray xfa = acroform.getAsArray(PdfName.XFA);
			for (int i = 0; i < xfa.size(); i += 2) {
				System.out.println("Reading: " + xfa.getAsString(i));
				PRStream s = (PRStream)xfa.getAsStream(i + 1);
				os.write(PdfReader.getStreamBytes(s));
			}
			os.flush();
			os.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
