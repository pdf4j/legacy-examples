/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.forms;
import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class FillEnabledFormBreakEnabling {

	public static final String ENABLED_FORM = "resources/questions/forms/enabled_form.pdf";
	public static final String RESULT = "results/questions/forms/enabled_form_broken.pdf";
	
	public static void main(String[] args) {
		try {
			PdfReader reader = new PdfReader(ENABLED_FORM);
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(RESULT));
			AcroFields form = stamper.getAcroFields();
			form.setField("form1[0].#subform[0].Body[0].EmployeeName[0]", "Bruno Lowagie");
			form.setField("form1[0].#subform[0].Body[0].Address[0]", "Ad. Baeyensstraat 121");
			form.setField("form1[0].#subform[0].Body[0].ZipCode[0]", "9040");
			form.setField("form1[0].#subform[0].Body[0].Comments[0]",
					"The example FillEnabledForm shows how NOT to prefill a Reader Enabled form. "
					+ "Using this code snippet will lead to a Reader warning saying: \"This document "
					+ "contained certain rights to enable special features in Adobe Reader. The document "
					+ "has been changed since it was created and these rights are no longer valid. Please "
					+ "contact the author for the original version of this document.\" You could also "
					+ "search the examples on http://1t3xt.info/examples/ to find out the correct way "
					+ "to fill a Reader Enabled form using iText.");
			stamper.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}