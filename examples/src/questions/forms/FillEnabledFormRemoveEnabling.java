/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.forms;
import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class FillEnabledFormRemoveEnabling {

	public static final String ENABLED_FORM = "resources/questions/forms/enabled_form.pdf";
	public static final String RESULT = "results/questions/forms/enabled_form_removed.pdf";
	
	public static void main(String[] args) {
		try {
			PdfReader reader = new PdfReader(ENABLED_FORM);
			reader.removeUsageRights();
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(RESULT));
			AcroFields form = stamper.getAcroFields();
			form.setField("form1[0].#subform[0].Body[0].EmployeeName[0]", "Bruno Lowagie");
			form.setField("form1[0].#subform[0].Body[0].Address[0]", "Ad. Baeyensstraat 121");
			form.setField("form1[0].#subform[0].Body[0].ZipCode[0]", "9040");
			form.setField("form1[0].#subform[0].Body[0].Comments[0]",
					"The example FillEnabledForm shows how to prefill a Reader Enabled form WITHOUT preserving the usage rights."
					+ " If you want to preserve the usage rights, look for the example FillEnabledForm.");
			stamper.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}