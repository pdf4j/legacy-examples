/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.compression;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.pdf.PdfWriter;

public class CompressionLevelsImage {

	public static final String RESOURCE = "resources/questions/img/logo.gif";
	public static final String[] RESULT = {
		"results/questions/compression/image_default_compression.pdf",
		"results/questions/compression/image_no_compression.pdf",
		"results/questions/compression/image_best_speed.pdf",
		"results/questions/compression/image_level_2.pdf",
		"results/questions/compression/image_level_3.pdf",
		"results/questions/compression/image_level_4.pdf",
		"results/questions/compression/image_level_5.pdf",
		"results/questions/compression/image_level_6.pdf",
		"results/questions/compression/image_level_7.pdf",
		"results/questions/compression/image_level_8.pdf",
		"results/questions/compression/image_best_compression.pdf"
	};
	
	public static void main(String[] args) {
		File file;
		for (int i = -1; i < 10; i++) {
			long before = new Date().getTime();
			createPdf(i);
			long after = new Date().getTime();
			file = new File(RESULT[i + 1]);
			System.out.println(file.getName() + "; time: " + (after - before)
					+ "ms; length: " + file.length() + " bytes");
		}
	}
	
	public static void createPdf(int compressionLevel) {
		try {
			Document document = new Document();
			PdfWriter writer = PdfWriter.getInstance(document,
					new FileOutputStream(RESULT[compressionLevel + 1]));
			writer.setCompressionLevel(compressionLevel);
			document.open();
			Image img = Image.getInstance(RESOURCE);
			img.setCompressionLevel(compressionLevel);
			document.add(img);
			document.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
