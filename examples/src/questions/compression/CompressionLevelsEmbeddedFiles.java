/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.compression;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfAnnotation;
import com.lowagie.text.pdf.PdfFileSpecification;
import com.lowagie.text.pdf.PdfWriter;

public class CompressionLevelsEmbeddedFiles {

	public static final String RESOURCE = "resources/questions/txt/caesar.txt";
	public static final String[] RESULT = {
		"results/questions/compression/attachment_default_compression.pdf",
		"results/questions/compression/attachment_no_compression.pdf",
		"results/questions/compression/attachment_best_speed.pdf",
		"results/questions/compression/attachment_level_2.pdf",
		"results/questions/compression/attachment_level_3.pdf",
		"results/questions/compression/attachment_level_4.pdf",
		"results/questions/compression/attachment_level_5.pdf",
		"results/questions/compression/attachment_level_6.pdf",
		"results/questions/compression/attachment_level_7.pdf",
		"results/questions/compression/attachment_level_8.pdf",
		"results/questions/compression/attachment_best_compression.pdf"
	};
	
	public static void main(String[] args) {
		File file;
		for (int i = -1; i < 10; i++) {
			long before = new Date().getTime();
			createPdf(i);
			long after = new Date().getTime();
			file = new File(RESULT[i + 1]);
			System.out.println(file.getName() + "; time: " + (after - before)
					+ "ms; length: " + file.length() + " bytes");
		}
	}
	
	public static void createPdf(int compressionLevel) {
		try {
			Document document = new Document();
			PdfWriter writer =
				PdfWriter.getInstance(document, new FileOutputStream(RESULT[compressionLevel + 1]));
			document.open();
			document.add(new Paragraph("Hello World"));
			PdfFileSpecification fs1 = PdfFileSpecification.fileEmbedded(
				writer, null, "some.txt", "some text".getBytes(), compressionLevel);
			writer.addAnnotation(PdfAnnotation.createFileAttachment(writer,
					new Rectangle(100f, 750f, 120f, 770f), "This is some text", fs1));
			PdfFileSpecification fs2 = PdfFileSpecification.fileEmbedded(
					writer, RESOURCE, "caesar.txt", null, compressionLevel);
			writer.addAnnotation(PdfAnnotation.createFileAttachment(writer,
					new Rectangle(100f, 780f, 120f, 800f), "Caesar", fs2));
			document.close();
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
	}
}
