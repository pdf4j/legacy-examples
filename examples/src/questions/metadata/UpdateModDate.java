/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.metadata;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;

public class UpdateModDate {

	public static final String ORIGINAL = "results/questions/metadata/original.pdf";
	public static final String MODIFIED = "results/questions/metadata/modified.pdf";
	
	public static void main(String[] args) {
		Document document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(
					document, new FileOutputStream(ORIGINAL));
			document.addTitle("Hello World example");
			document.addSubject("This example shows how to add metadata");
			document.addKeywords("Metadata, iText, XMP");
			document.addCreator("My program using iText");
			document.addAuthor("Bruno Lowagie");
			writer.createXmpMetadata();
			document.open();
			document.add(new Paragraph("Hello World"));
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		document.close();
		
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		PdfReader reader;
		try {
			reader = new PdfReader(ORIGINAL);
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(MODIFIED));
			stamper.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
