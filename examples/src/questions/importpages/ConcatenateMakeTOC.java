/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.importpages;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfWriter;

public class ConcatenateMakeTOC {

	public static final String[] RESULTS =
		{ "results/questions/importpages/doc1.pdf",
		  "results/questions/importpages/doc2.pdf",
		  "results/questions/importpages/doc3.pdf" };
	public static final String RESULT =
		"results/questions/importpages/doc123_with_toc.pdf";
	
	public static void main(String[] args) {
		Document document = new Document();
		try {
			PdfCopy copy = new PdfCopy(document, new FileOutputStream(RESULT));
			copy.setViewerPreferences(PdfWriter.PageModeUseOutlines);
			document.open();
			List<HashMap<String, Object>> bookmarks = new ArrayList<HashMap<String, Object>>();
			List<HashMap<String, Object>> kids;
			PdfReader reader;
			int page = 1;
			for (int i = 0; i < 3; i++) {
				createPdf(i);
				HashMap<String, Object> titlepage = new HashMap<String, Object>();
				titlepage.put("Title", String.format("Document %d", i + 1));
				titlepage.put("Action", "GoTo");
				titlepage.put("Page", String.format("%d Fit", page));
				kids = new ArrayList<HashMap<String, Object>>();
				reader = new PdfReader(RESULTS[i]);
				for (int j = 1; j <= reader.getNumberOfPages(); j++) {
					copy.addPage(copy.getImportedPage(reader, j));
					HashMap<String, Object> kid = new HashMap<String, Object>();
					kid.put("Title", String.format("Page %d in document %d", j, i + 1));
					kid.put("Action", "GoTo");
					kid.put("Page", String.format("%d FitH 806", page));
					kids.add(kid);
					page++;
				}
				titlepage.put("Kids", kids);
				bookmarks.add(titlepage);
			}
			copy.setOutlines(bookmarks);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}
	
	public static void createPdf(int i) {
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream(RESULTS[i]));
			document.open();
			int doc = i + 1;
			for (i = 1; i < 4; i++) {
				document.add(new Paragraph(String.format("Document %d; page %d", doc, i)));
				document.newPage();
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}
}
