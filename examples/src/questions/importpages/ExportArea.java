/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.importpages;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfRectangle;
import com.lowagie.text.pdf.PdfStamper;

public class ExportArea {

	public static final String SOURCE = "resources/questions/pdfs/metro.pdf";
	public static final String RESULT = "results/questions/importpages/area.pdf";
	
	public static void main(String[] args) {
		try {
			PdfReader reader = new PdfReader(SOURCE);
			PdfDictionary pagedict = reader.getPageN(1);
			PdfRectangle rect = new PdfRectangle(45, 610, 603, 906);
			pagedict.put(PdfName.MEDIABOX, rect);
			pagedict.put(PdfName.CROPBOX, rect);
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(RESULT));
			stamper.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}

