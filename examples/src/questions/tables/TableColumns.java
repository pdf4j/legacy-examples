/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.tables;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class TableColumns {

	public static final String RESULT = "results/questions/tables/table_columns.pdf";
	
	public static void main(String[] args) {
		Document document = new Document(PageSize.A4.rotate());
		try {
			PdfWriter writer =
				PdfWriter.getInstance(document, new FileOutputStream(RESULT));
			
			document.open();
			
			// the content of the columns
			ColumnText content = new ColumnText(writer.getDirectContent());
			PdfPTable items = new PdfPTable(2);
			items.setWidthPercentage(100);
			for (int i = 0; i < 100; ++i) {
				items.addCell("item " + i);
				items.addCell("some item");
			}
			content.addElement(items);
			
			// adding the stuff to the document
			int column = 0;
			float height = 0;
			float[][] x = {
					{ document.left(), document.left() + 380 },
					{ document.right() - 380, document.right() }
				};
			int status = ColumnText.START_COLUMN;
			while (ColumnText.hasMoreText(status)) {
				if (column == 0) {
					PdfPTable table = new PdfPTable(1);
					table.setWidthPercentage(100);
					table.addCell("EmployeeSheets");
					table.addCell("Page " + writer.getPageNumber());
					document.add(table);
					height = table.getTotalHeight();
				}
				content.setSimpleColumn(
						x[column][0], document.bottom(),
						x[column][1], document.top() - height - 10);
				status = content.go();
				if (++column >= x.length) {
					column = 0;
					document.newPage();
				}
			}
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		document.close();
	}
}
