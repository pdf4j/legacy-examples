/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.tables;

import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.html.simpleparser.StyleSheet;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class TableAndHTMLWorker {

	public static final String RESOURCE = "resources/questions/html/test.html";
	public static final String RESULT = "results/questions/tables/list_in_table.pdf";
	
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		Document document = new Document();
		try {
			StyleSheet styles = new StyleSheet();
			PdfWriter.getInstance(document, new FileOutputStream(RESULT));
			document.open();
			ArrayList objects;
			objects = HTMLWorker.parseToList(new FileReader(RESOURCE), styles);
			PdfPTable table = new PdfPTable(1);
			PdfPCell cell = new PdfPCell();
			for (int k = 0; k < objects.size(); ++k)
				cell.addElement((Element) objects.get(k));
			table.addCell(cell);
			document.add(table);
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println(e.getMessage());
		}
		document.close();
	}
}
