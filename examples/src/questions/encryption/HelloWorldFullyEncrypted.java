/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.encryption;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfAnnotation;
import com.lowagie.text.pdf.PdfWriter;

public class HelloWorldFullyEncrypted {
	public static final String RESULT = "results/questions/encryption/everything_encrypted.pdf";
	
	public static void main(String[] args) {
		// step 1
		Document document = new Document();
		try {
			// step 2
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(RESULT));
			writer.setEncryption("hello".getBytes(), "world".getBytes(), 0, PdfWriter.ENCRYPTION_AES_128);
			writer.createXmpMetadata();
			// step 3
			document.open();
			// step 4
			document.add(new Paragraph("Hello World"));
			writer.addAnnotation(PdfAnnotation.createFileAttachment(writer,
					new Rectangle(100f, 650f, 150f, 700f), "This is some text",
					"some text".getBytes(), null, "some.txt"));
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		// step 5
		document.close();
	}
}
