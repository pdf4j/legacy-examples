/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.separators;

import java.io.FileOutputStream;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.LineSeparator;

public class SeparatedWords1 {

	public static final String RESULT = "results/questions/separators/columns.pdf";
	
    public static void main(String[] args) {
        Document document = new Document();
        try {
            PdfWriter.getInstance(document,
                    new FileOutputStream(RESULT));
            document.open();
            
        	Chunk separator = new Chunk(new LineSeparator());
        	
            Paragraph p;
            for (int i = 1000; i < 1040; i++) {
            	p = new Paragraph("TEST", new Font(Font.COURIER));
            	p.add(separator);
            	p.add(new Chunk(String.valueOf(i)));
            	p.add(separator);
            	p.add(new Chunk(String.valueOf(i + 100)));
            	p.add(separator);
            	p.add(new Chunk(String.valueOf(i + 200)));
            	p.add(separator);
            	p.add(new Chunk(String.valueOf(i + 300)));
            	document.add(p);
            }
            document.close();
        }
        catch (Exception de)
        {
            de.printStackTrace();
        }
    }
}
