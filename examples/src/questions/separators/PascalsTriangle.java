/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.separators;

import java.io.FileOutputStream;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.VerticalPositionMark;

public class PascalsTriangle {

	public static final String RESULT = "results/questions/separators/pascal.pdf";
	
    public static void main(String[] args) {
        Document document = new Document();
        try {
            PdfWriter.getInstance(document,
                    new FileOutputStream(RESULT));
            document.open();
            
        	Chunk separator = new Chunk(new VerticalPositionMark());
            int[] line = { 0, 1, 0 };
        	
            Paragraph p;
            int n;
            int[] tmp;
            for (int i = 0; i < 20; i++) {
            	p = new Paragraph(separator);
            	n = line.length;
            	tmp = new int[n + 1];
            	tmp[0] = line[0];
            	tmp[n] = line[n - 1];
            	tmp[n - 1] = line[n - 2];
            	for (int j = 1; j < n - 1; j++) {
            		p.add(new Chunk(String.valueOf(line[j])));
            		p.add(separator);
            		tmp[j] = line[j] + line[j - 1];
            	}
            	document.add(p);
            	line = tmp;
            }
            document.close();
        }
        catch (Exception de)
        {
            de.printStackTrace();
        }
    }
}
