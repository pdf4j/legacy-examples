/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.separators;

import java.io.FileOutputStream;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.DottedLineSeparator;

public class DottedGlue {

	public static final String RESULT = "results/questions/separators/dotted_lines.pdf";
	
    public static void main(String[] args) {
        Document document = new Document();
        try {
            PdfWriter writer = PdfWriter.getInstance(document,
                    new FileOutputStream(RESULT));
            document.open();
            
            PdfContentByte canvas = writer.getDirectContent();
            canvas.moveTo(document.left(), document.top());
            canvas.lineTo(document.left(), 550);
            canvas.moveTo(document.right(), document.top());
            canvas.lineTo(document.right(), 550);
            canvas.stroke();
            
        	Chunk separator = new Chunk(new DottedLineSeparator());
            
        	// two parts
            Paragraph p1 = new Paragraph();
            p1.add(new Chunk("Chapter 1"));
            p1.add(separator);
            p1.add(new Chunk("p 10"));

            // 3 parts
            Paragraph p2 = new Paragraph();
            p2.add(new Chunk("Chapter 2"));
            p2.add(separator);
            p2.add(new Chunk("x"));
            p2.add(separator);
            p2.add(new Chunk("y"));
            
            // line ends with separator
            Paragraph p3 = new Paragraph();
            p3.add(new Chunk("Chapter 3"));
            p3.add(separator);

            // line starts with a separator
            Paragraph p4 = new Paragraph();
            p4.add(separator);
            p4.add(new Chunk("chapter 4"));
            
            // line IS a separator
            Paragraph p5 = new Paragraph();
            p5.add(separator);
            
            // line IS a separator + leading
            Paragraph p6 = new Paragraph(40);
            p6.add(separator);
            
            // separator galore
            Paragraph p7 = new Paragraph();
            p7.setAlignment(Element.ALIGN_JUSTIFIED);
            for (int i = 0; i < 10; i++) {
            	p7.add(new Chunk("This is a test hippopotamus hippopotamus"));
            	if (i % 4 == 0)
            		p7.add(" hippopotamus hippopotamus hippopotamus hippopotamus");
            	p7.add(separator);
            }

            document.add(p1);
            document.add(p1);
            document.add(p2);
            document.add(p3);
            document.add(p4);
            document.add(p5);
            document.add(p6);
            document.add(p7);

            ColumnText ct = new ColumnText(writer.getDirectContent());
            ct.addElement(p1);
            ct.addElement(p1);
            ct.addElement(p2);
            ct.addElement(p3);
            ct.addElement(p4);
            ct.addElement(p5);
            ct.addElement(p6);
            ct.addElement(p7);
            ct.setSimpleColumn(36, 36, 436, 536);
            ct.go();
            canvas.rectangle(36, 36, 400, 500);
            canvas.stroke();
            document.close();
        }
        catch (Exception de)
        {
            de.printStackTrace();
        }
    }
}
