/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.separators;

import java.io.FileOutputStream;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.DottedLineSeparator;
import com.lowagie.text.pdf.draw.LineSeparator;
import com.lowagie.text.pdf.draw.VerticalPositionMark;

public class TabbedWords2 {

	public static final String RESULT = "results/questions/separators/tabbed_words2.pdf";
	
    public static void main(String[] args) {
        Document document = new Document();
        try {
            PdfWriter writer = PdfWriter.getInstance(document,
                    new FileOutputStream(RESULT));
            document.open();
        	
            Paragraph p;
        	Chunk tab1 = new Chunk(new VerticalPositionMark(), 100, true);
        	Chunk tab2 = new Chunk(new LineSeparator(), 120, true);
        	Chunk tab3 = new Chunk(new DottedLineSeparator(), 200, true);
        	Chunk tab4 = new Chunk(new VerticalPositionMark(), 240, true);
        	Chunk tab5 = new Chunk(new VerticalPositionMark(), 300, true);
            
        	PdfContentByte canvas = writer.getDirectContent();
        	ColumnText column = new ColumnText(canvas);
        	for (int i = 0; i < 40; i++) {
            	p = new Paragraph("TEST");
            	p.add(tab1);
            	p.add(new Chunk(String.valueOf(i)));
            	p.add(tab2);
            	p.add(new Chunk(String.valueOf(i * 2)));
            	p.add(tab3);
            	p.add(new Chunk(SeparatedWords2.WORDS[39 - i]));
            	p.add(tab4);
            	p.add(new Chunk(String.valueOf(i * 4)));
            	p.add(tab5);
            	p.add(new Chunk(SeparatedWords2.WORDS[i]));
            	column.addElement(p);
            }
        	column.setSimpleColumn(60, 36, 400, 806);
        	canvas.moveTo(60, 36);
        	canvas.lineTo(60, 806);
        	canvas.moveTo(160, 36);
        	canvas.lineTo(160, 806);
        	canvas.moveTo(180, 36);
        	canvas.lineTo(180, 806);
        	canvas.moveTo(260, 36);
        	canvas.lineTo(260, 806);
        	canvas.moveTo(300, 36);
        	canvas.lineTo(300, 806);
        	canvas.moveTo(360, 36);
        	canvas.lineTo(360, 806);
        	canvas.moveTo(400, 36);
        	canvas.lineTo(400, 806);
        	canvas.stroke();
        	column.go();

        	document.setMargins(60, 195, 36, 36);
        	document.newPage();

        	canvas.moveTo(document.left(), document.bottom());
        	canvas.lineTo(document.left(), document.top());
        	canvas.moveTo(document.right(), document.bottom());
        	canvas.lineTo(document.right(), document.top());
        	canvas.stroke();
            for (int i = 0; i < 40; i++) {
            	p = new Paragraph("TEST");
            	p.add(tab1);
            	p.add(new Chunk(String.valueOf(i)));
            	p.add(tab2);
            	p.add(new Chunk(String.valueOf(i * 2)));
            	p.add(tab3);
            	p.add(new Chunk(SeparatedWords2.WORDS[39 - i]));
            	p.add(tab4);
            	p.add(new Chunk(String.valueOf(i * 4)));
            	p.add(tab5);
            	p.add(new Chunk(SeparatedWords2.WORDS[i]));
            	document.add(p);
            }
            document.close();
        }
        catch (Exception de)
        {
            de.printStackTrace();
        }
    }
}
