/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.separators;

import java.io.FileOutputStream;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.DottedLineSeparator;
import com.lowagie.text.pdf.draw.LineSeparator;
import com.lowagie.text.pdf.draw.VerticalPositionMark;

public class TOCExample {
	public static final String RESULT = "results/questions/separators/toc.pdf";
	
    public static void main(String[] args) {
        Document document = new Document();
        try {
            PdfWriter writer = PdfWriter.getInstance(document,
                    new FileOutputStream(RESULT));
            document.open();

        	Chunk tab0 = new Chunk(new VerticalPositionMark(), 0, true);
        	Chunk tab1 = new Chunk(new LineSeparator(), 40, true);
        	Chunk tab2 = new Chunk(new DottedLineSeparator(), 120, true);

        	Paragraph p1 = new Paragraph();
        	p1.add(new Chunk("1"));
            p1.add(tab1);
            p1.add(new Chunk("Chapter 1"));
            p1.add(tab2);
            p1.add(new Chunk("p1"));
        	Paragraph p2 = new Paragraph();
            p2.add(tab1);
            p2.add(new Chunk("Introduction"));
            p2.add(tab2);
            p2.add(new Chunk("p2"));
        	Paragraph p3 = new Paragraph();
            p3.add(tab1);
            p3.add(new Chunk("Let us test the tab functionality in iText with a long line"));
            p3.add(tab2);
            p3.add(new Chunk("p3"));
        	Paragraph p4 = new Paragraph();
            p4.add(tab1);
            p4.add(new Chunk("test"));
            p4.add(tab2);
        	Paragraph p5 = new Paragraph();
        	p5.add(new Chunk("2"));
            p5.add(tab1);
            p5.add(tab2);
        	Paragraph p6 = new Paragraph();
            p6.add(tab1);
            p6.add(tab2);
            p6.add(new Chunk("p4"));
        	Paragraph p7 = new Paragraph();
            p7.add(tab1);
            p7.add(tab2);
        	Paragraph p8 = new Paragraph();
        	p8.add(new Chunk("3"));
            p8.add(tab1);
            p8.add(new Chunk("Chapter 3"));
            p8.add(tab2);
            p8.add(new Chunk("p5"));
            p8.add(tab0);
        	p8.add(new Chunk("4"));
            p8.add(tab1);
            p8.add(new Chunk("Chapter 4 - the final chapter"));
            p8.add(tab2);
            p8.add(new Chunk("p6"));
            
            document.add(p1);
            document.add(p2);
            document.add(p3);
            document.add(p4);
            document.add(p5);
            document.add(p6);
            document.add(p7);
            document.add(p8);
        
            PdfContentByte canvas = writer.getDirectContent();
            ColumnText column = new ColumnText(canvas);
            column.addElement(p1);
            column.addElement(p2);
            column.addElement(p3);
            column.addElement(p4);
            column.addElement(p5);
            column.addElement(p6);
            column.addElement(p7);
            column.addElement(p8);
            column.setSimpleColumn(36, 500, 200, 600);
            column.go();
            column.setSimpleColumn(260, 500, 460, 600);
            column.go();
         
            document.close();
        }
        catch (Exception de)
        {
            de.printStackTrace();
        }
    }
}
