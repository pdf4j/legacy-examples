/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.separators;

import java.awt.Color;
import java.io.FileOutputStream;

import com.lowagie.text.Document;
import com.lowagie.text.Element;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.draw.LineSeparator;
import com.lowagie.text.pdf.draw.VerticalPositionMark;

public class LineSeparator2 {

	public static final String RESULT = "results/questions/separators/line_separator2.pdf";
	
    public static void main(String[] args) {
        Document document = new Document();
        try {
            PdfWriter writer = PdfWriter.getInstance(document,
                    new FileOutputStream(RESULT));
            document.open();
            
        	VerticalPositionMark separator = new LineSeparator(1, 80, Color.RED, Element.ALIGN_RIGHT, -2);

            ColumnText column = new ColumnText(writer.getDirectContent());
            for (int i = 0; i < 5; i++) {
            	column.addElement(StarSeparators.TEXT);
            	column.addElement(separator);
            }
            column.setSimpleColumn(36, 36, 295, 806);
            column.go();
            column.setSimpleColumn(300, 36, 559, 806);
            column.go();
            document.newPage();
        	
        	for (int i = 0; i < 10; i++) {
            	document.add(StarSeparators.TEXT);
            	document.add(separator);
            }
            document.close();
        }
        catch (Exception de)
        {
            de.printStackTrace();
        }
    }
}
