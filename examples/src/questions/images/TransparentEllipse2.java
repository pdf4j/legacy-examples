/*
 * This example was written based on code provided on the mailing list
 * by Mark Storer. It was adapted by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.images;

import java.io.*;
import com.lowagie.text.*;
import com.lowagie.text.pdf.*;

public class TransparentEllipse2 {
	public static final String RESULT = "results/questions/images/transparent_ellipse2.pdf";
	public static final String RESOURCE = "resources/questions/img/bruno_original.jpg";
	
    public static void main(String[] args) {
    	
        Document document = new Document(PageSize.POSTCARD);
        try {
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(RESULT));
            document.open();
            PdfContentByte cb = writer.getDirectContent();

            // clipped image
            cb.ellipse(1, 1, PageSize.POSTCARD.getWidth() - 2, PageSize.POSTCARD.getHeight() - 2);
            cb.clip();
            cb.newPath();
            Image img = Image.getInstance(RESOURCE);
			img.scaleToFit(PageSize.POSTCARD.getWidth(), PageSize.POSTCARD.getHeight());
            cb.addImage(img, PageSize.POSTCARD.getWidth(), 0, 0, PageSize.POSTCARD.getHeight(), 0, 0);

            //Prepare gradation list
            int gradationStep = 40;
            float[] gradationRatioList = new float[gradationStep];
            for(int i = 0; i < gradationStep; i++) {
                gradationRatioList[i] = 1 - (float)Math.sin(Math.toRadians(90.0f / gradationStep * (i + 1)));
            }
            
            //Create template
            PdfTemplate template = cb.createTemplate(PageSize.POSTCARD.getWidth(), PageSize.POSTCARD.getHeight());
            
            //Prepare transparent group
            PdfTransparencyGroup transGroup = new PdfTransparencyGroup();
            transGroup.put( PdfName.CS, PdfName.DEVICEGRAY);
            transGroup.setIsolated(true);
            transGroup.setKnockout(false);
            template.setGroup(transGroup);
            
            //Prepare graphic state
            PdfGState gState = new PdfGState();
            PdfDictionary maskDict = new PdfDictionary();
            maskDict.put( PdfName.TYPE, PdfName.MASK );
            maskDict.put( PdfName.S, new PdfName( "Luminosity" ) );
            maskDict.put( new PdfName("G"), template.getIndirectReference() );
            gState.put( PdfName.SMASK, maskDict );
            cb.setGState(gState);
            
            //Create gradation for mask
            for(int i = 1; i < gradationStep + 1; i++) {
                template.setLineWidth(gradationStep + 1 - i);
                template.setGrayStroke(gradationRatioList[gradationStep - i]);
                template.ellipse(0, 0, PageSize.POSTCARD.getWidth(), PageSize.POSTCARD.getHeight());
                template.stroke();
            }
            
            //Place template
            cb.addTemplate(template, 0, 0);
        }
        catch(DocumentException de) {
            System.err.println(de.getMessage());
        }
        catch(IOException ioe) {
            System.err.println(ioe.getMessage());
        }
        
        document.close();
    }
}
