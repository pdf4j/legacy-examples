/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.images;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfWriter;

public class OverlappingImages {

	public static final String RESULT = "results/questions/images/overlapping.pdf";
	public static final String RESOURCE1 = "resources/questions/img/bruno_original.jpg";
	public static final String RESOURCE2 = "resources/questions/img/1t3xt.gif";
	public static final String RESOURCE3 = "resources/questions/img/logo.gif";
	
	public static void main(String[] args) {
		// step 1: creation of a document-object
		Document document = new Document(PageSize.POSTCARD);
		try {
			// step 2:
			// we create a writer
			PdfWriter.getInstance(
					// that listens to the document
					document,
					// and directs a PDF-stream to a file
					new FileOutputStream(RESULT));
			// step 3: we open the document
			document.open();
			// step 4: we add a paragraph to the document
			Image img1 = Image.getInstance(RESOURCE1);
			img1.scaleToFit(PageSize.POSTCARD.getWidth(), 10000);
			img1.setAbsolutePosition(0, 0);
			document.add(img1);
			Image img2 = Image.getInstance(RESOURCE2);
			img2.setTransparency(new int[]{ 0x00, 0x10 });
			img2.setAbsolutePosition(
					PageSize.POSTCARD.getWidth() - img2.getScaledWidth(),
					PageSize.POSTCARD.getHeight() - img2.getScaledHeight());
			document.add(img2);
			Image img3 = Image.getInstance(RESOURCE3);
			img3.setTransparency(new int[]{ 0xF0, 0xFF });
			img3.setAbsolutePosition(
					(PageSize.POSTCARD.getWidth() - img3.getScaledWidth()) / 2,
					(PageSize.POSTCARD.getHeight() - img3.getScaledHeight()) / 2);
			document.add(img3);
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}

		// step 5: we close the document
		document.close();
		
	}
}
