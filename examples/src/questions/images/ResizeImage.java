/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.images;

import java.awt.Graphics2D;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.imageio.ImageIO;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PRStream;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfObject;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;

public class ResizeImage {

	public static final String RESOURCE = "resources/questions/img/bruno_original.jpg";
	public static final String ORIGINAL = "results/questions/images/original_image_size.pdf";
	public static final String RESULT = "results/questions/images/reduced_image_size.pdf";
	
	public static void main(String[] args) throws IOException, DocumentException {
		createPdf(ORIGINAL);
		PdfReader reader = new PdfReader(ORIGINAL);
		resizeImagesInPage(reader, 1, 0.5f);
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(RESULT));
        stamper.close();
	}
	
	public static void createPdf(String filename) throws IOException, DocumentException {
		Document document = new Document(PageSize.A2);
		PdfWriter.getInstance(document,
				new FileOutputStream(filename));
		document.open();
		Image img = Image.getInstance(RESOURCE);
		document.add(img);
		document.close();
	}
	
	public static void resizeImagesInPage(PdfReader reader, int page, float factor) throws IOException {	
		PdfDictionary pageDictionary = reader.getPageN(page);
		PdfDictionary resources = (PdfDictionary)PdfReader.getPdfObject(
				pageDictionary.get(PdfName.RESOURCES));
        PdfDictionary xobjects = (PdfDictionary)PdfReader.getPdfObject(
        		resources.get(PdfName.XOBJECT));
        if (xobjects != null) {
            for(Object xObjectKey : xobjects.getKeys()) {
                PdfName xobjectName = (PdfName)xObjectKey;
                PdfObject xobj = xobjects.get(xobjectName);
                PRStream imageStream = (PRStream)PdfReader.getPdfObject(xobj);
                PdfName subtype = (PdfName)PdfReader.getPdfObject(imageStream.get(PdfName.SUBTYPE));
                PdfName filter = (PdfName)PdfReader.getPdfObject(imageStream.get(PdfName.FILTER));
                if  (PdfName.IMAGE.equals(subtype) && PdfName.DCTDECODE.equals(filter)) {
                    BufferedImage src = ImageIO.read(new ByteArrayInputStream(PdfReader.getStreamBytesRaw(imageStream)));
                    int width = (int)(src.getWidth() * factor);
                    int height = (int)(src.getHeight() * factor);
                    BufferedImage dest = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
                    AffineTransform at = AffineTransform.getScaleInstance(factor, factor);
                    Graphics2D g = dest.createGraphics();
                    g.drawRenderedImage(src,at);
                    ByteArrayOutputStream newImage = new ByteArrayOutputStream();
                    ImageIO.write(dest, "JPG", newImage);
                    imageStream.setData(newImage.toByteArray(), false, PRStream.NO_COMPRESSION);
                    imageStream.put(PdfName.WIDTH, new PdfNumber(width));
                    imageStream.put(PdfName.HEIGHT, new PdfNumber(height));
                    imageStream.put(PdfName.FILTER, PdfName.DCTDECODE);
                }
            }
        }
	}
}
