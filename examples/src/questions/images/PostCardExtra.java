/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.images;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfImage;
import com.lowagie.text.pdf.PdfIndirectObject;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfWriter;

public class PostCardExtra {
	
	public static final String RESULT = "results/questions/images/postcard_special.pdf";
	public static final String RESOURCE = "resources/questions/img/bruno_original.jpg";
	
	public static void main(String[] args) {
		// step 1: creation of a document-object
		Document document = new Document(PageSize.POSTCARD);
		try {
			// step 2:
			// we create a writer
			PdfWriter writer = PdfWriter.getInstance(
					// that listens to the document
					document,
					// and directs a PDF-stream to a file
					new FileOutputStream(RESULT));
			// step 3: we open the document
			document.open();
			// step 4: we add a paragraph to the document
			Image img = Image.getInstance(RESOURCE);
			img.scaleToFit(PageSize.POSTCARD.getWidth(), 10000);
			img.setAbsolutePosition(0, 0);
			PdfImage stream = new PdfImage(img, "", null);
			stream.put(new PdfName("MySpecialId"), new PdfName("123456789"));
			PdfIndirectObject ref = writer.addToBody(stream);
			img.setDirectReference(ref.getIndirectReference());
			document.add(img);
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}

		// step 5: we close the document
		document.close();
	}
}
