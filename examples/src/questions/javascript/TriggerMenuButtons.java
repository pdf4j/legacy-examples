/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.javascript;

import java.awt.Color;
import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfAction;
import com.lowagie.text.pdf.PdfFormField;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.PushbuttonField;

public class TriggerMenuButtons {

	public static final String RESULT = "results/questions/javascript/trigger_menu_buttons.pdf";
	
	public static final void main(String[] args) {
		Document document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(RESULT));
			document.open();
			document.add(new Paragraph("Triggering Menu Items"));
			PushbuttonField saveAs = new PushbuttonField(writer, new Rectangle(
					40, 760, 100, 780), "Save");
			saveAs.setBorderColor(Color.BLACK);
			saveAs.setText("Save");
			saveAs.setTextColor(Color.RED);
			saveAs.setLayout(PushbuttonField.LAYOUT_LABEL_ONLY);
			PdfFormField saveAsButton = saveAs.getField();
			saveAsButton.setAction(PdfAction.javaScript("app.execMenuItem('SaveAs')",
					writer));
			writer.addAnnotation(saveAsButton);
			PushbuttonField mail = new PushbuttonField(writer, new Rectangle(
					120, 760, 180, 780), "Mail");
			mail.setBorderColor(Color.BLACK);
			mail.setText("Mail");
			mail.setTextColor(Color.RED);
			mail.setLayout(PushbuttonField.LAYOUT_LABEL_ONLY);
			PdfFormField mailButton = mail.getField();
			mailButton.setAction(PdfAction.javaScript("app.execMenuItem('AcroSendMail:SendMail')",
					writer));
			writer.addAnnotation(mailButton);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}
}
