/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.javascript;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.PdfWriter;

public class VersionChecker {

	public static final String RESULT = "results/questions/javascript/version_checker.pdf";
	
	public static final void main(String[] args) {
		Document document = new Document();
		try {
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(RESULT));
			document.open();
			writer.addJavaScript(
					"app.alert('Viewer: ' + app.viewerType + '; Version: ' + app.viewerVersion);");
			String myPhrase = "If JavaScript is allowed, an alert will show you the version of your viewer";
			Paragraph p = new Paragraph (new Phrase(myPhrase));
			document.add(p);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
		document.close();
	}
}
