/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.stamppages;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.AcroFields;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;

public class StampAndAddColumns {

	public static final String RESOURCE = "resources/questions/forms/hello_who.pdf";
	public static final String TXT = "resources/questions/txt/caesar.txt";
	public static final String RESULT = "results/questions/stamppages/stamp_columns.pdf";
	
	public static void main(String[] args) {
		try {
			PdfReader reader = new PdfReader(RESOURCE);
			PdfStamper stamper = new PdfStamper(reader,
					new FileOutputStream(RESULT));
			AcroFields form = stamper.getAcroFields();
			form.setField("Who", "world");
			ColumnText ct = new ColumnText(stamper.getOverContent(1));
			String line;
			Phrase p;
			BufferedReader br = new BufferedReader(new FileReader(TXT));
			while ((line = br.readLine()) != null) {
				p = new Phrase(line + "\n");
				ct.addText(p);
			}
			int status = ColumnText.START_COLUMN;
			ct.setSimpleColumn(100, 700, 495, 100);
			status = ct.go();
			int pageCt = 1;
			while (ColumnText.hasMoreText(status)) {
				stamper.insertPage(++pageCt, reader.getPageSize(1));
				ct.setYLine(700);
				ct.setCanvas(stamper.getOverContent(pageCt));
				status = ct.go();
			}
			stamper.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
