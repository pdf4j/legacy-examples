/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.stamppages;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfDictionary;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfRectangle;
import com.lowagie.text.pdf.PdfStamper;

public class IncreaseMediabox {
	public static final String RESOURCE = "resources/questions/pdfs/photo_album.pdf";
	public static final String RESULT = "results/questions/stamppages/new_album.pdf";
		
	public static void main(String[] args) {
		try {
			PdfDictionary pageDict;
			PdfReader reader = new PdfReader(RESOURCE);
			int n = reader.getNumberOfPages();
			for (int i = 1; i <= n; i++) {
				Rectangle rect = reader.getPageSize(i);
				pageDict = reader.getPageN(i);
				PdfRectangle pdfrect = new PdfRectangle(
						rect.getLeft(-36), rect.getBottom(-36), rect.getRight(-36), rect.getTop(-36));
				pageDict.put(PdfName.MEDIABOX, pdfrect);
			}
			PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(RESULT));
			stamper.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (DocumentException e) {
			e.printStackTrace();
		}
	}
}
