/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.stamppages;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfName;
import com.lowagie.text.pdf.PdfNumber;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;

public class ChangeViewerPreferences {
	public static final String RESOURCE = "resources/questions/pdfs/hello.pdf";
	public static final String RESULT = "results/questions/stamppages/world.pdf";

	public static void main(String[] args) throws IOException, DocumentException {
		PdfReader reader = new PdfReader(RESOURCE);
		PdfStamper stamper = new PdfStamper(reader, new FileOutputStream(RESULT));
		stamper.setViewerPreferences(PdfWriter.PageModeUseThumbs);
		stamper.addViewerPreference(PdfName.NUMCOPIES, new PdfNumber(5));
		stamper.close();
	}
}
