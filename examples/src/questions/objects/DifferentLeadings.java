/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.objects;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.Font;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfWriter;

public class DifferentLeadings {
	public static final String RESULT = "results/questions/objects/different_leadings.pdf";
	
	public static void main(String[] args) {
		Document document = new Document(PageSize.A7);
		try {
			PdfWriter writer = PdfWriter.getInstance(
					document,
					new FileOutputStream(RESULT));
			document.open();
			Chunk space = new Chunk(' ');
			String text = "Quick brown fox jumps over the lazy dog.";
			Phrase phrase1 = new Phrase(text, new Font(Font.HELVETICA, 12));
			Phrase phrase2 = new Phrase(new Chunk(text, new Font(Font.TIMES_ROMAN, 24)));
			Phrase phrase3 = new Phrase(text, new Font(Font.COURIER, 8));
			Phrase phrase4 = new Phrase(text, new Font(Font.HELVETICA, 4));
			Paragraph paragraph = new Paragraph();
			paragraph.add(phrase1);
			paragraph.add(space);
			paragraph.add(phrase2);
			paragraph.add(space);
			paragraph.add(phrase3);
			paragraph.add(space);
			paragraph.add(phrase4);
			paragraph.setMultipliedLeading(1.5f);
			paragraph.setAlignment(Element.ALIGN_JUSTIFIED);
			ColumnText column = new ColumnText(writer.getDirectContent());
			column.setSimpleColumn(document.left(), document.bottom(),
					document.right(), document.top());
			column.addElement(paragraph);
			column.go();
			document.newPage();
			document.add(paragraph);
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		document.close();
	}
}
