/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.objects;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfWriter;

public class NewPageColumns {
	public static final String RESULT = "results/questions/objects/new_page_columns.pdf";
	
	public static void main(String[] args) {
		
		// step 1
		Document document = new Document(PageSize.A6);
		try {
			// step 2
			PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(RESULT));
			// step 3
			document.open();
			// step 4
			for (int i = 0; i < 25; i++)
				document.add(new Paragraph("Hello paragraph 1." + i));
			document.newPage();
			ColumnText column = new ColumnText(writer.getDirectContent());
			column.setSimpleColumn(PageSize.A6.getLeft(36), PageSize.A6.getBottom(36),
					PageSize.A6.getRight(36), PageSize.A6.getTop(36));
			for (int i = 0; i < 20; i++)
				column.addElement(new Paragraph("Hello column 1." + i));
			int status = column.go();
			while (ColumnText.hasMoreText(status)) {
				document.newPage();
				column.setYLine(PageSize.A6.getTop(36));
				status = column.go();
			}
			document.newPage();
			for (int i = 0; i < 10; i++)
				column.addElement(new Paragraph("Hello column 2." + i));
			status = column.go();
			while (ColumnText.hasMoreText(status)) {
				document.newPage();
				column.setYLine(PageSize.A6.getTop(36));
				status = column.go();
			}
			document.newPage();
			for (int i = 0; i < 5; i++)
				document.add(new Paragraph("Hello paragraph 2." + i));
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		// step 5
		document.close();
	}
}
