/*
 * This example was written for Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.objects;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.List;
import com.lowagie.text.ListItem;
import com.lowagie.text.pdf.PdfWriter;

public class NestingLists {

	public static final String RESULT = "results/questions/objects/nesting_lists.pdf";
	
	public static void main(String[] args) throws IOException, DocumentException {
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream(RESULT));
		document.open();
		List list1 = new List(List.ORDERED, 20);
        list1.add(new ListItem(new Chunk("Level 1 - Item 1")));
        list1.add(new ListItem(new Chunk("Level 1 - Item 2")));
        list1.add(new ListItem(new Chunk("Level 1 - Item 3")));

        List list2 = new List(List.ORDERED, 20);
        list2.add(new ListItem(new Chunk("Level 2 - Item 1")));
        list2.add(new ListItem(new Chunk("Level 2 - Item 2")));

        List list3 = new List(List.ORDERED, 20);
        list3.add(new ListItem(new Chunk("Level 3 - Item 1")));
        list3.add(new ListItem(new Chunk("Level 3 - Item 2")));
        list3.add(new ListItem(new Chunk("Level 3 - Item 3")));
        list3.add(new ListItem(new Chunk("Level 3 - Item 4")));
        list2.add(list3); 

        list1.add(list2);
        list1.add(new ListItem(new Chunk("Level 1 - Item 4")));

        document.add(list1);
		document.close();
	}
}
