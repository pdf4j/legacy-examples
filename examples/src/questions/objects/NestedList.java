/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.objects;

import java.io.FileOutputStream;
import java.io.IOException;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.List;
import com.lowagie.text.ListItem;
import com.lowagie.text.pdf.PdfWriter;

public class NestedList {
	
	public static final String RESULT = "results/questions/objects/nested_list.pdf";

	public static void main(String[] args) {
		Document document = new Document();
		try {
			PdfWriter.getInstance(document, new FileOutputStream(RESULT));
			document.open();
			List list;
			ListItem listItem;
			List sublist;
			list = new List(true);
			list.setPreSymbol("Chapter ");
			list.setPostSymbol(": ");
			listItem = new ListItem("Isaac Asimov");
			list.add(listItem);
			sublist = new List(true);
			sublist.setIndentationLeft(10);
			sublist.setPreSymbol("1.");
			sublist.add("The Foundation Trilogy");
			sublist.add("The Complete Robot");
			sublist.add("Caves of Steel");
			sublist.add("The Naked Sun");
			list.add(sublist);
			listItem = new ListItem("John Irving");
			list.add(listItem);
			sublist = new List(true);
			sublist.setIndentationLeft(10);
			sublist.setPreSymbol("Section 2.");
			sublist.add("The World according to Garp");
			sublist.add("Hotel New Hampshire");
			sublist.add("A prayer for Owen Meany");
			sublist.add("Widow for a year");
			list.add(sublist);
			listItem = new ListItem("Kurt Vonnegut");
			list.add(listItem);
			sublist = new List(true);
			sublist.setIndentationLeft(10);
			sublist.setPreSymbol("3.");
			sublist.setPostSymbol("# ");
			sublist.add("Slaughterhouse 5");
			sublist.add("Welcome to the Monkey House");
			sublist.add("The great pianola");
			sublist.add("Galapagos");
			list.add(sublist);
			document.add(list);
		} catch (DocumentException de) {
			System.err.println(de.getMessage());
		} catch (IOException ioe) {
			System.err.println(ioe.getMessage());
		}
		document.close();
	}
}
