/*
 * This example was written by Bruno Lowagie, author of the book
 * 'iText in Action' by Manning Publications (ISBN: 1932394796).
 * You can use this example as inspiration for your own applications.
 * The following license applies:
 * http://www.1t3xt.com/about/copyright/index.php?page=MIT
 */

package questions.objects;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;

import com.lowagie.text.Chapter;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Section;
import com.lowagie.text.Table;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;

public class MemoryTests {

	public static final String RESULT0 = "results/questions/objects/test_results.txt";
	public static final String RESULT1 = "results/questions/objects/chapters_without_memory_management.pdf";
	public static final String RESULT2 = "results/questions/objects/chapters_with_memory_management.pdf";
	public static final String RESULT3 = "results/questions/objects/pdfptable_without_memory_management.pdf";
	public static final String RESULT4 = "results/questions/objects/pdfptable_with_memory_management.pdf";
	public static final String RESULT5 = "results/questions/objects/table_without_memory_management.pdf";
	public static final String RESULT6 = "results/questions/objects/table_with_memory_management.pdf";
	
	private boolean test;
	private long memory_use;
	private long initial_memory_use = 0l;
	private long maximum_memory_use = 0l;
	
	public static void main(String[] args) {
		MemoryTests tests = new MemoryTests();
		tests.createPdfs();
	}
	
	public void createPdfs() {
		try {
			PrintWriter writer = new PrintWriter(new FileOutputStream(RESULT0));
			resetMaximum(writer);
			test = false;
			println(writer, RESULT1);
			createPdfWithChapters(writer, RESULT1);
			resetMaximum(writer);
			test = true;
			println(writer, RESULT2);
			createPdfWithChapters(writer, RESULT2);
			resetMaximum(writer);
			test = false;
			println(writer, RESULT3);
			createPdfWithPdfPTable(writer, RESULT3);
			resetMaximum(writer);
			test = true;
			println(writer, RESULT4);
			createPdfWithPdfPTable(writer, RESULT4);
			resetMaximum(writer);
			test = false;
			println(writer, RESULT5);
			createPdfWithTable(writer, RESULT5);
			resetMaximum(writer);
			test = true;
			println(writer, RESULT6);
			createPdfWithTable(writer, RESULT6);
			resetMaximum(writer);
			writer.flush();
			writer.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	private void createPdfWithChapters(Writer writer, String filename) throws FileNotFoundException, DocumentException {
		// step 1: creation of a document-object
		Document document = new Document();
		// step 2: we create a writer
		PdfWriter.getInstance(
				document,
				new FileOutputStream(filename));
		// step 3: we open the document
		document.open();
		// step 4: we add content to the document
		Chapter chapter;
		Section section;
		Section subsection;
		chapter = new Chapter(new Paragraph("chapter", new Font(Font.HELVETICA, 14, Font.BOLD)), 1);
		if(test) chapter.setComplete(false);
		addContent(chapter, 0);
		if (test) document.add(chapter);
		checkpoint(writer);
		section = chapter.addSection("section 1", 1);
		if(test) section.setComplete(false);
		addContent(section, 1);
		if (test) document.add(chapter);
		checkpoint(writer);
		subsection = section.addSection("subsection 1", 2);
		addContent(subsection, 2);
		if (test) document.add(chapter);
		checkpoint(writer);
		subsection = section.addSection("subsection 2", 2);
		addContent(subsection, 3);
		if (test) document.add(chapter);
		checkpoint(writer);
		subsection = section.addSection("subsection 3", 2);
		addContent(subsection, 4);
		if (test) document.add(chapter);
		checkpoint(writer);
		section = chapter.addSection("section 2", 1);
		if(test) section.setComplete(false);
		subsection = section.addSection("subsection 1", 2);
		addContent(subsection, 5);
		if (test) document.add(chapter);
		checkpoint(writer);
		subsection = section.addSection("subsection 2", 2);
		addContent(subsection, 6);
		if (test) chapter.setComplete(true);
		document.add(chapter);
		checkpoint(writer);
		// step 5: we close the document
		document.close();
	}

	private void createPdfWithTable(Writer writer, String filename) throws FileNotFoundException, DocumentException {
		// step 1
		Document document = new Document();
		// step 2
		PdfWriter.getInstance(document, new FileOutputStream(filename));
		// step 3: we open the document
		document.open();
		// step 4
		Table table = new Table(2);
		table.addCell("Header1");
		table.addCell("Header2");
		table.endHeaders();
		if (test) table.setComplete(false);
		for (int i = 0; i < 500; i++) {
			if (i % 50 == 50 - 1) {
				if (test) document.add(table);
				checkpoint(writer);
			}
			table.addCell(String.valueOf(i));
			table.addCell("Quick brown fox jumps over the lazy dog.");
		}
		if (test) table.setComplete(true);
		document.add(table);
		checkpoint(writer);
		// step 5
		document.close();
	}

	private void createPdfWithPdfPTable(Writer writer, String filename) throws FileNotFoundException, DocumentException {
		// step 1
		Document document = new Document();
		// step 2
		PdfWriter.getInstance(document, new FileOutputStream(filename));
		// step 3: we open the document
		document.open();
		// step 4
		PdfPTable table = new PdfPTable(2);
		table.setHeaderRows(1);
		table.addCell("Header1");
		table.addCell("Header2");
		if (test) table.setComplete(false);
		for (int i= 1; i <= 500; i++) {
			if (i % 50 == 50 - 1) {
				if (test) document.add(table);
				checkpoint(writer);
			}
			table.addCell(String.valueOf(i));
			table.addCell("Quick brown fox jumps over the lazy dog.");
		}
		if (test) table.setComplete(true);
		document.add(table);
		checkpoint(writer);
		// step 5
		document.close();
	}
	
	private void addContent(Section section, int s) {
		for (int i = 1; i < 100; i++) {
			StringBuffer buf = new StringBuffer("(");
			buf.append(i);
			buf.append(") ");
			buf.append(BART_SIMPSON[s]);
			section.add(new Paragraph(buf.toString()));
		}
	}
	
	private void checkpoint(Writer writer) throws DocumentException {
		memory_use = getMemoryUse();
		maximum_memory_use = Math.max(maximum_memory_use, memory_use);
		println(writer, "memory use: ", memory_use);
	}
	
	private void resetMaximum(Writer writer) {
		println(writer, "maximum: ", maximum_memory_use);
		println(writer, "total used: ", maximum_memory_use - initial_memory_use);
		maximum_memory_use = 0l;
		initial_memory_use = getMemoryUse();
		println(writer, "initial memory use: ", initial_memory_use);
	}
	
	private void println(Writer writer, String message) {
		try {
			writer.write(message);
			writer.write("\n");
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void println(Writer writer, String message, long l) {
		try {
			writer.write(message + l);
			writer.write("\n");
			writer.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns the current memory use.
	 * 
	 * @return the current memory use
	 */
	private static long getMemoryUse() {
		garbageCollect();
		garbageCollect();
		long totalMemory = Runtime.getRuntime().totalMemory();
		garbageCollect();
		garbageCollect();
		long freeMemory = Runtime.getRuntime().freeMemory();
		return (totalMemory - freeMemory);
	}
	
	/**
	 * Makes sure all garbage is cleared from the memory.
	 */
	private static void garbageCollect() {
		try {
			System.gc();
			Thread.sleep(100);
			System.runFinalization();
			Thread.sleep(100);
			System.gc();
			Thread.sleep(100);
			System.runFinalization();
			Thread.sleep(100);
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}
	}
	public static final String[] BART_SIMPSON =
	{
		"I will not bribe Principal Skinner.",
		"\"Bart Bucks\" are not legal tender.",
		"There are plenty of businesses like show business.",
		"I did not see Elvis.",
		"A burp is not an answer.",
		"I will not celebrate meaningless milestones.",
		"I will not go very far with this attitude."
	};
}
