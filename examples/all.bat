javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter01/HelloWorld.java
java -cp "bin;lib/iText.jar" in_action.chapter01.HelloWorld
javac -d bin -cp "bin;lib/iText.jar;lib/iText-toolbox.jar" src/in_action/chapter01/HelloWorldBurst.java
java -cp "bin;lib/iText.jar;lib/iText-toolbox.jar" in_action.chapter01.HelloWorldBurst
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorld.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorld
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldNarrow.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldNarrow
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldLetter.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldLetter
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldLandscape.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldLandscape
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldLandscape2.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldLandscape2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldBlue.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldBlue
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldMargins.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldMargins
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldMirroredMargins.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldMirroredMargins
javac -d bin -cp "bin;lib/iText.jar;lib/iText-rtf.jar" src/in_action/chapter02/HelloWorldMultiple.java
java -cp "bin;lib/iText.jar;lib/iText-rtf.jar" in_action.chapter02.HelloWorldMultiple
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldSystemOut.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldSystemOut
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldOpen.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldOpen
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldVersion_1_6.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldVersion_1_6
javac -d bin -cp "bin;lib/iText.jar;lib/iText-rtf.jar" src/in_action/chapter02/HelloWorldMetadata.java
java -cp "bin;lib/iText.jar;lib/iText-rtf.jar" in_action.chapter02.HelloWorldMetadata
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldAbsolute.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldAbsolute
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldGraphics2D.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldGraphics2D
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldReader.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldReader
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldPartialReader.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldPartialReader
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldBookmarks.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldBookmarks
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldReadMetadata.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldReadMetadata
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldAddMetadata.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldAddMetadata
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldForm.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldForm
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldStamper.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldStamper
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldStamper2.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldStamper2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldStamperAdvanced.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldStamperAdvanced
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldStamperImportedPages.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldStamperImportedPages
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldImportedPages.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldImportedPages
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldWriter.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldWriter
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldCopy.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldCopy
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldSelectPages.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldSelectPages
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldSelectedPages.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldSelectedPages
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldCopyForm.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldCopyForm
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldCopyFields.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldCopyFields
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldStampCopy.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldStampCopy
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldCopyStamp.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldCopyStamp
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldStampCopyStamp.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldStampCopyStamp
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter03/HelloWorldMaximum.java
java -cp "bin;lib/iText.jar" in_action.chapter03.HelloWorldMaximum
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter03/HelloWorldUncompressed.java
java -cp "bin;lib/iText.jar" in_action.chapter03.HelloWorldUncompressed
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter03/HelloWorldFullyCompressed.java
java -cp "bin;lib/iText.jar" in_action.chapter03.HelloWorldFullyCompressed
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter03/HelloWorldCompression.java
java -cp "bin;lib/iText.jar" in_action.chapter03.HelloWorldCompression
javac -d bin -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" src/in_action/chapter03/HelloWorldEncryptDecrypt.java
java -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" in_action.chapter03.HelloWorldEncryptDecrypt
javac -d bin -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" src/in_action/chapter03/HelloWorldEncrypted.java
java -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" in_action.chapter03.HelloWorldEncrypted
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogChunk1.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogChunk1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogChunk2.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogChunk2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogPhrase.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogPhrase
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogParagraph.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogParagraph
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogAnchor1.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogAnchor1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogAnchor2.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogAnchor2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogList1.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogList1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogList2.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogList2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogChapter1.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogChapter1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogChapter2.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogChapter2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogScale.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogScale
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogUnderline.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogUnderline
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogSupSubscript.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogSupSubscript
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogSkew.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogSkew
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogColor.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogColor
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogRender.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogRender
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogSplit.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogSplit
javac -d bin -cp "bin;lib/iText.jar;lib/itext-hyph-xml.jar" src/in_action/chapter04/DickensHyphenated.java
java -cp "bin;lib/iText.jar;lib/itext-hyph-xml.jar" in_action.chapter04.DickensHyphenated
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogSpaceCharRatio.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogSpaceCharRatio
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogGoto1.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogGoto1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogGoto2.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogGoto2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogGoto3.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogGoto3
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogGoto4.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogGoto4
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogGeneric1.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogGeneric1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogGeneric2.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogGeneric2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoxDogGeneric3.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoxDogGeneric3
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter04/FoobarFlyer.java
java -cp "bin;lib/iText.jar" in_action.chapter04.FoobarFlyer
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoxDogImageTypes.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoxDogImageTypes
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoxDogMultipageTiff.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoxDogMultipageTiff
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoxDogAnimatedGif.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoxDogAnimatedGif
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/HitchcockAwtImage.java
java -cp "bin;lib/iText.jar" in_action.chapter05.HitchcockAwtImage
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/HitchcockAwt.java
java -cp "bin;lib/iText.jar" in_action.chapter05.HitchcockAwt
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoxDogRawImage.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoxDogRawImage
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/Barcodes.java
java -cp "bin;lib/iText.jar" in_action.chapter05.Barcodes
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldImportedPages.java
java -cp "bin;lib/iText.jar" in_action.chapter02.HelloWorldImportedPages
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoxDogImageAlignment.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoxDogImageAlignment
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoxDogImageWrapping.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoxDogImageWrapping
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoxDogImageChunk.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoxDogImageChunk
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoxDogImageRectangle.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoxDogImageRectangle
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoxDogImageSequence.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoxDogImageSequence
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoxDogImageTranslation.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoxDogImageTranslation
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoxDogImageScaling1.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoxDogImageScaling1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoxDogImageScaling2.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoxDogImageScaling2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoxDogImageRotation.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoxDogImageRotation
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoxDogImageMask.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoxDogImageMask
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/FoobarFlyer.java
java -cp "bin;lib/iText.jar" in_action.chapter05.FoobarFlyer
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/MyFirstPdfPTable.java
java -cp "bin;lib/iText.jar" in_action.chapter06.MyFirstPdfPTable
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableAligned.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableAligned
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableAbsoluteWidth.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableAbsoluteWidth
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableColumnWidths.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableColumnWidths
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableAbsoluteWidths.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableAbsoluteWidths
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableAbsoluteColumns.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableAbsoluteColumns
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableSpacing.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableSpacing
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableWithoutBorders.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableWithoutBorders
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableCellAlignment.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableCellAlignment
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableCellSpacing.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableCellSpacing
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableCellHeights.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableCellHeights
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableColors.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableColors
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableVerticalCells.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableVerticalCells
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableNested.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableNested
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableImages.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableImages
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableSplit.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableSplit
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableRepeatHeader.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableRepeatHeader
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableRepeatHeaderFooter.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableRepeatHeaderFooter
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableMemoryFriendly.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableMemoryFriendly
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableCompare.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableCompare
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableAbsolutePositions.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableAbsolutePositions
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/PdfPTableSplitVertically.java
java -cp "bin;lib/iText.jar" in_action.chapter06.PdfPTableSplitVertically
javac -d bin -cp "bin;lib/iText.jar;lib/iText-rtf.jar" src/in_action/chapter06/MyFirstTable.java
java -cp "bin;lib/iText.jar;lib/iText-rtf.jar" in_action.chapter06.MyFirstTable
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/SpecificCells.java
java -cp "bin;lib/iText.jar" in_action.chapter06.SpecificCells
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter06/FoobarStudyProgram.java
java -cp "bin;lib/iText.jar" in_action.chapter06.FoobarStudyProgram
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/ParagraphText.java
java -cp "bin;lib/iText.jar" in_action.chapter07.ParagraphText
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/ParagraphPositions.java
java -cp "bin;lib/iText.jar" in_action.chapter07.ParagraphPositions
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/ColumnWithAddText.java
java -cp "bin;lib/iText.jar" in_action.chapter07.ColumnWithAddText
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/ColumnWithSetSimpleColumn.java
java -cp "bin;lib/iText.jar" in_action.chapter07.ColumnWithSetSimpleColumn
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/ColumnWithSetText.java
java -cp "bin;lib/iText.jar" in_action.chapter07.ColumnWithSetText
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/ColumnControl.java
java -cp "bin;lib/iText.jar" in_action.chapter07.ColumnControl
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/ColumnsRegular.java
java -cp "bin;lib/iText.jar" in_action.chapter07.ColumnsRegular
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/ColumnsIrregular.java
java -cp "bin;lib/iText.jar" in_action.chapter07.ColumnsIrregular
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/ColumnProperties.java
java -cp "bin;lib/iText.jar" in_action.chapter07.ColumnProperties
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/ColumnElements.java
java -cp "bin;lib/iText.jar" in_action.chapter07.ColumnElements
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/ColumnWithAddElement.java
java -cp "bin;lib/iText.jar" in_action.chapter07.ColumnWithAddElement
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/MultiColumnPoem.java
java -cp "bin;lib/iText.jar" in_action.chapter07.MultiColumnPoem
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/MultiColumnPoemReverse.java
java -cp "bin;lib/iText.jar" in_action.chapter07.MultiColumnPoemReverse
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/MultiColumnPoemCustom.java
java -cp "bin;lib/iText.jar" in_action.chapter07.MultiColumnPoemCustom
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/MultiColumnIrregular.java
java -cp "bin;lib/iText.jar" in_action.chapter07.MultiColumnIrregular
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter07/FoobarCourseCatalog.java
java -cp "bin;lib/iText.jar" in_action.chapter07.FoobarCourseCatalog
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter08/StandardType1Fonts.java
java -cp "bin;lib/iText.jar" in_action.chapter08.StandardType1Fonts
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter08/FontMetrics.java
java -cp "bin;lib/iText.jar" in_action.chapter08.FontMetrics
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter08/StandardType1FontFromAFM.java
java -cp "bin;lib/iText.jar" in_action.chapter08.StandardType1FontFromAFM
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter08/Type1FontFromAFM.java
java -cp "bin;lib/iText.jar" in_action.chapter08.Type1FontFromAFM
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter08/Type1FontFromPFBwithAFM.java
java -cp "bin;lib/iText.jar" in_action.chapter08.Type1FontFromPFBwithAFM
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter08/Type1FontFromPFBwithPFM.java
java -cp "bin;lib/iText.jar" in_action.chapter08.Type1FontFromPFBwithPFM
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter08/Type3Characters.java
java -cp "bin;lib/iText.jar" in_action.chapter08.Type3Characters
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter08/TrueTypeFontExample.java
java -cp "bin;lib/iText.jar" in_action.chapter08.TrueTypeFontExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter08/CompactFontFormatExample.java
java -cp "bin;lib/iText.jar" in_action.chapter08.CompactFontFormatExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter08/TrueTypeFontEncoding.java
java -cp "bin;lib/iText.jar" in_action.chapter08.TrueTypeFontEncoding
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter08/FileSizeComparison.java
java -cp "bin;lib/iText.jar" in_action.chapter08.FileSizeComparison
javac -d bin -cp "bin;lib/iText.jar;lib/iTextAsian.jar" src/in_action/chapter08/ChineseKoreanJapaneseFonts.java
java -cp "bin;lib/iText.jar;lib/iTextAsian.jar" in_action.chapter08.ChineseKoreanJapaneseFonts
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter08/CIDTrueTypeOutlines.java
java -cp "bin;lib/iText.jar" in_action.chapter08.CIDTrueTypeOutlines
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter08/TrueTypeCollections.java
java -cp "bin;lib/iText.jar" in_action.chapter08.TrueTypeCollections
javac -d bin -cp "bin;lib/iText.jar;lib/iTextAsian.jar" src/in_action/chapter09/VerticalTextExample.java
java -cp "bin;lib/iText.jar;lib/iTextAsian.jar" in_action.chapter09.VerticalTextExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter09/RightToLeftExample.java
java -cp "bin;lib/iText.jar" in_action.chapter09.RightToLeftExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter09/SayPeace.java
java -cp "bin;lib/iText.jar" in_action.chapter09.SayPeace
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter09/Diacritics1.java
java -cp "bin;lib/iText.jar" in_action.chapter09.Diacritics1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter09/Diacritics2.java
java -cp "bin;lib/iText.jar" in_action.chapter09.Diacritics2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter09/Monospace.java
java -cp "bin;lib/iText.jar" in_action.chapter09.Monospace
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter09/Ligatures1.java
java -cp "bin;lib/iText.jar" in_action.chapter09.Ligatures1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter09/Ligatures2.java
java -cp "bin;lib/iText.jar" in_action.chapter09.Ligatures2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter09/FontFactoryExample1.java
java -cp "bin;lib/iText.jar" in_action.chapter09.FontFactoryExample1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter09/FontFactoryExample2.java
java -cp "bin;lib/iText.jar" in_action.chapter09.FontFactoryExample2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter09/SymbolSubstitution.java
java -cp "bin;lib/iText.jar" in_action.chapter09.SymbolSubstitution
javac -d bin -cp "bin;lib/iText.jar;lib/iTextAsian.jar" src/in_action/chapter09/FontSelectionExample.java
java -cp "bin;lib/iText.jar;lib/iTextAsian.jar" in_action.chapter09.FontSelectionExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter09/Peace.java
java -cp "bin;lib/iText.jar" in_action.chapter09.Peace
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/InvisibleRectangles.java
java -cp "bin;lib/iText.jar" in_action.chapter10.InvisibleRectangles
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/ConstructingPaths1.java
java -cp "bin;lib/iText.jar" in_action.chapter10.ConstructingPaths1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/ConstructingPaths2.java
java -cp "bin;lib/iText.jar" in_action.chapter10.ConstructingPaths2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/ConstructingPaths3.java
java -cp "bin;lib/iText.jar" in_action.chapter10.ConstructingPaths3
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/ConstructingPaths4.java
java -cp "bin;lib/iText.jar" in_action.chapter10.ConstructingPaths4
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/DirectContent.java
java -cp "bin;lib/iText.jar" in_action.chapter10.DirectContent
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/PdfPTableCellEvents.java
java -cp "bin;lib/iText.jar" in_action.chapter10.PdfPTableCellEvents
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/PdfPTableEvents.java
java -cp "bin;lib/iText.jar" in_action.chapter10.PdfPTableEvents
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/PdfPTableFloatingBoxes.java
java -cp "bin;lib/iText.jar" in_action.chapter10.PdfPTableFloatingBoxes
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/GraphicsStateStack.java
java -cp "bin;lib/iText.jar" in_action.chapter10.GraphicsStateStack
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/LineCharacteristics.java
java -cp "bin;lib/iText.jar" in_action.chapter10.LineCharacteristics
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/EyeLogo.java
java -cp "bin;lib/iText.jar" in_action.chapter10.EyeLogo
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/EyeCoordinates.java
java -cp "bin;lib/iText.jar" in_action.chapter10.EyeCoordinates
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/EyeImages.java
java -cp "bin;lib/iText.jar" in_action.chapter10.EyeImages
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/EyeInlineImage.java
java -cp "bin;lib/iText.jar" in_action.chapter10.EyeInlineImage
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/FoobarSvgHandler.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/FoobarCity.java
java -cp "bin;lib/iText.jar" in_action.chapter10.FoobarCity
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter11/DeviceColor.java
java -cp "bin;lib/iText.jar" in_action.chapter11.DeviceColor
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter11/SeparationColor.java
java -cp "bin;lib/iText.jar" in_action.chapter11.SeparationColor
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter11/Patterns.java
java -cp "bin;lib/iText.jar" in_action.chapter11.Patterns
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter11/ShadingPatterns.java
java -cp "bin;lib/iText.jar" in_action.chapter11.ShadingPatterns
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter11/ColoredParagraphs.java
java -cp "bin;lib/iText.jar" in_action.chapter11.ColoredParagraphs
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter11/Transparency1.java
java -cp "bin;lib/iText.jar" in_action.chapter11.Transparency1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter11/Transparency2.java
java -cp "bin;lib/iText.jar" in_action.chapter11.Transparency2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter11/Transparency3.java
java -cp "bin;lib/iText.jar" in_action.chapter11.Transparency3
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldForm.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter11/TemplateClip.java
java -cp "bin;lib/iText.jar" in_action.chapter11.TemplateClip
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter11/ClippingPath.java
java -cp "bin;lib/iText.jar" in_action.chapter11.ClippingPath
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter11/TextOperators.java
java -cp "bin;lib/iText.jar" in_action.chapter11.TextOperators
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter11/TextMethods.java
java -cp "bin;lib/iText.jar" in_action.chapter11.TextMethods
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter10/FoobarSvgHandler.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter11/FoobarSvgTextHandler.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter11/FoobarCityStreets.java
java -cp "bin;lib/iText.jar" in_action.chapter11.FoobarCityStreets
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter12/SunTutorialExample.java
java -cp "bin;lib/iText.jar" in_action.chapter12.SunTutorialExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter12/SunTutorialExample.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter12/SunTutorialExampleWithText.java
java -cp "bin;lib/iText.jar" in_action.chapter12.SunTutorialExampleWithText
javac -d bin -cp "bin;lib/iText.jar;lib/iTextAsian.jar" src/in_action/chapter12/JapaneseExample1.java
java -cp "bin;lib/iText.jar;lib/iTextAsian.jar" in_action.chapter12.JapaneseExample1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter12/JapaneseExample2.java
java -cp "bin;lib/iText.jar" in_action.chapter12.JapaneseExample2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter12/HindiExample.java
java -cp "bin;lib/iText.jar" in_action.chapter12.HindiExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter12/MyJTable.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter12/JTextPaneToPdf.java
javac -d bin -cp "bin;lib/iText.jar;lib/jfreechart.jar;lib/jcommon.jar" src/in_action/chapter12/FoobarCharts.java
java -cp "bin;lib/iText.jar;lib/jfreechart.jar;lib/jcommon.jar" in_action.chapter12.FoobarCharts
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter12/PeekABoo.java
java -cp "bin;lib/iText.jar" in_action.chapter12.PeekABoo
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter12/OptionalContentExample.java
java -cp "bin;lib/iText.jar" in_action.chapter12.OptionalContentExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter12/LayerMembershipExample.java
java -cp "bin;lib/iText.jar" in_action.chapter12.LayerMembershipExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter12/OptionalContentActionExample.java
java -cp "bin;lib/iText.jar" in_action.chapter12.OptionalContentActionExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter12/OptionalXObjectExample.java
java -cp "bin;lib/iText.jar" in_action.chapter12.OptionalXObjectExample
javac -d bin -cp "bin;lib/iText.jar;lib/batik-awt-util.jar;lib/batik-bridge.jar;lib/batik-css.jar;lib/batik-util.jar;lib/batik-ext.jar;lib/batik-gvt.jar;lib/batik-dom.jar;lib/batik-parser.jar;lib/batik-script.jar;lib/batik-svg-dom.jar;lib/batik-xml.jar;lib/xerces_2_5_0.jar" src/in_action/chapter12/FoobarCityBatik.java
java -cp "bin;lib/iText.jar;lib/batik-awt-util.jar;lib/batik-bridge.jar;lib/batik-css.jar;lib/batik-util.jar;lib/batik-ext.jar;lib/batik-gvt.jar;lib/batik-dom.jar;lib/batik-parser.jar;lib/batik-script.jar;lib/batik-svg-dom.jar;lib/batik-xml.jar;lib/xerces_2_5_0.jar" in_action.chapter12.FoobarCityBatik
javac -d bin -cp "bin;lib/iText.jar;lib/batik-awt-util.jar;lib/batik-bridge.jar;lib/batik-css.jar;lib/batik-util.jar;lib/batik-ext.jar;lib/batik-gvt.jar;lib/batik-dom.jar;lib/batik-parser.jar;lib/batik-script.jar;lib/batik-svg-dom.jar;lib/batik-xml.jar;lib/xerces_2_5_0.jar" src/in_action/chapter12/FoobarCityBatik.java
java -cp "bin;lib/iText.jar;lib/batik-awt-util.jar;lib/batik-bridge.jar;lib/batik-css.jar;lib/batik-util.jar;lib/batik-ext.jar;lib/batik-gvt.jar;lib/batik-dom.jar;lib/batik-parser.jar;lib/batik-script.jar;lib/batik-svg-dom.jar;lib/batik-xml.jar;lib/xerces_2_5_0.jar" in_action.chapter12.FoobarCityBatik
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/VPPageLayout.java
java -cp "bin;lib/iText.jar" in_action.chapter13.VPPageLayout
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/VPPageModeAndLayout.java
java -cp "bin;lib/iText.jar" in_action.chapter13.VPPageModeAndLayout
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/VPExamples.java
java -cp "bin;lib/iText.jar" in_action.chapter13.VPExamples
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/PageLabels.java
java -cp "bin;lib/iText.jar" in_action.chapter13.PageLabels
javac -d bin -cp "bin;lib/iText.jar;lib/iText-toolbox.jar" src/in_action/chapter13/PhotoThumbs.java
java -cp "bin;lib/iText.jar;lib/iText-toolbox.jar" in_action.chapter13.PhotoThumbs
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/ThumbImage.java
java -cp "bin;lib/iText.jar" in_action.chapter13.ThumbImage
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/SlideShow.java
java -cp "bin;lib/iText.jar" in_action.chapter13.SlideShow
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/ExplicitDestinations.java
java -cp "bin;lib/iText.jar" in_action.chapter13.ExplicitDestinations
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/OutlineActions.java
java -cp "bin;lib/iText.jar" in_action.chapter13.OutlineActions
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/HelloWorldManipulateBookmarks.java
java -cp "bin;lib/iText.jar" in_action.chapter13.HelloWorldManipulateBookmarks
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/HelloWorldCopyBookmarks.java
java -cp "bin;lib/iText.jar" in_action.chapter13.HelloWorldCopyBookmarks
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/NamedActions.java
java -cp "bin;lib/iText.jar" in_action.chapter13.NamedActions
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/GotoActions.java
java -cp "bin;lib/iText.jar" in_action.chapter13.GotoActions
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/EventTriggeredActions.java
java -cp "bin;lib/iText.jar" in_action.chapter13.EventTriggeredActions
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/DocumentLevelJavaScript.java
java -cp "bin;lib/iText.jar" in_action.chapter13.DocumentLevelJavaScript
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/LaunchAction.java
java -cp "bin;lib/iText.jar" in_action.chapter13.LaunchAction
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/CourseCatalogBookmarked.java
java -cp "bin;lib/iText.jar" in_action.chapter13.CourseCatalogBookmarked
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/EmptyPages.java
java -cp "bin;lib/iText.jar" in_action.chapter14.EmptyPages
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/PageBoundaries.java
java -cp "bin;lib/iText.jar" in_action.chapter14.PageBoundaries
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/ReorderPages.java
java -cp "bin;lib/iText.jar" in_action.chapter14.ReorderPages
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/HeaderFooterExample.java
java -cp "bin;lib/iText.jar" in_action.chapter14.HeaderFooterExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/PageXofY.java
java -cp "bin;lib/iText.jar" in_action.chapter14.PageXofY
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/WatermarkExample.java
java -cp "bin;lib/iText.jar" in_action.chapter14.WatermarkExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/SlideShow.java
java -cp "bin;lib/iText.jar" in_action.chapter14.SlideShow
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/ParagraphOutlines.java
java -cp "bin;lib/iText.jar" in_action.chapter14.ParagraphOutlines
javac -d bin -cp "bin;lib/iText.jar;lib/iText-toolbox.jar" src/in_action/chapter14/ChapterEvents.java
java -cp "bin;lib/iText.jar;lib/iText-toolbox.jar" in_action.chapter14.ChapterEvents
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/SimpleLetter.java
java -cp "bin;lib/iText.jar" in_action.chapter14.SimpleLetter
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/SimpleLetter.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/SimpleLetters.java
java -cp "bin;lib/iText.jar" in_action.chapter14.SimpleLetters
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/RomeoJuliet.java
java -cp "bin;lib/iText.jar" in_action.chapter14.RomeoJuliet
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/HtmlParseExample.java
java -cp "bin;lib/iText.jar" in_action.chapter14.HtmlParseExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/ParsingHtml.java
java -cp "bin;lib/iText.jar" in_action.chapter14.ParsingHtml
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/ParsingHtmlSnippets.java
java -cp "bin;lib/iText.jar" in_action.chapter14.ParsingHtmlSnippets
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter13/CourseCatalogBookmarked.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter14/CourseCatalogEvents.java
java -cp "bin;lib/iText.jar" in_action.chapter14.CourseCatalogEvents
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter15/SimpleAnnotations.java
java -cp "bin;lib/iText.jar" in_action.chapter15.SimpleAnnotations
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter15/TextAnnotations.java
java -cp "bin;lib/iText.jar" in_action.chapter15.TextAnnotations
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter15/Annotations.java
java -cp "bin;lib/iText.jar" in_action.chapter15.Annotations
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter15/AnnotatedChunks.java
java -cp "bin;lib/iText.jar" in_action.chapter15.AnnotatedChunks
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter15/AnnotatedImages.java
java -cp "bin;lib/iText.jar" in_action.chapter15.AnnotatedImages
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter15/Buttons.java
java -cp "bin;lib/iText.jar" in_action.chapter15.Buttons
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter15/Buttons2.java
java -cp "bin;lib/iText.jar" in_action.chapter15.Buttons2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter15/TextFields.java
java -cp "bin;lib/iText.jar" in_action.chapter15.TextFields
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter15/ChoiceFields.java
java -cp "bin;lib/iText.jar" in_action.chapter15.ChoiceFields
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter15/SenderReceiver.java
java -cp "bin;lib/iText.jar" in_action.chapter15.SenderReceiver
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter15/Calculator.java
java -cp "bin;lib/iText.jar" in_action.chapter15.Calculator
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter15/FieldActions.java
java -cp "bin;lib/iText.jar" in_action.chapter15.FieldActions
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter16/RegisterForm1.java
java -cp "bin;lib/iText.jar" in_action.chapter16.RegisterForm1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter16/RegisterForm1.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter16/FillAcroForm1.java
java -cp "bin;lib/iText.jar" in_action.chapter16.FillAcroForm1
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter16/FillAcroForm2.java
java -cp "bin;lib/iText.jar" in_action.chapter16.FillAcroForm2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter16/RegisterForm1.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter16/FillAcroForm3.java
java -cp "bin;lib/iText.jar" in_action.chapter16.FillAcroForm3
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter16/UnsignedSignatureField.java
java -cp "bin;lib/iText.jar" in_action.chapter16.UnsignedSignatureField
javac -d bin -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" src/in_action/chapter16/UnsignedSignatureField.java
javac -d bin -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" src/in_action/chapter16/SignedSignatureField.java
java -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" in_action.chapter16.SignedSignatureField
javac -d bin -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" src/in_action/chapter16/SignedPdf.java
java -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" in_action.chapter16.SignedPdf
javac -d bin -cp "bin;lib/iText.jar;lib/servlet.jar;lib/iText-rtf.jar" src/in_action/chapter17/HelloWorldServlet.java
javac -d bin -cp "bin;lib/iText.jar;lib/servlet.jar" src/in_action/chapter17/OutSimplePdf.java
javac -d bin -cp "bin;lib/iText.jar;lib/servlet.jar" src/in_action/chapter17/ProgressServlet.java
javac -d bin -cp "bin;lib/iText.jar;lib/servlet.jar" src/in_action/chapter17/FoobarCourses.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter17/FoobarLearningAgreement.java
java -cp "bin;lib/iText.jar" in_action.chapter17.FoobarLearningAgreement
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter18/HelloWorld.java
java -cp "bin;lib/iText.jar" in_action.chapter18.HelloWorld
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter18/ClimbTheTree.java
java -cp "bin;lib/iText.jar" in_action.chapter18.ClimbTheTree
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter18/HelloWorldStream.java
java -cp "bin;lib/iText.jar" in_action.chapter18.HelloWorldStream
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter18/HelloWorldReverse.java
java -cp "bin;lib/iText.jar" in_action.chapter18.HelloWorldReverse
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter18/HelloWorldStreamHack.java
java -cp "bin;lib/iText.jar" in_action.chapter18.HelloWorldStreamHack
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter18/SilentPrinting.java
java -cp "bin;lib/iText.jar" in_action.chapter18.SilentPrinting
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter18/ChangeURL.java
java -cp "bin;lib/iText.jar" in_action.chapter18.ChangeURL
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter05/Barcodes.java
java -cp "bin;lib/iText.jar" in_action.chapter05.Barcodes
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterF/HelloWorldPdfX.java
java -cp "bin;lib/iText.jar" in_action.chapterF.HelloWorldPdfX
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterF/HelloWorldXmpMetadata.java
java -cp "bin;lib/iText.jar" in_action.chapterF.HelloWorldXmpMetadata
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterF/HelloWorldXmpMetadata2.java
java -cp "bin;lib/iText.jar" in_action.chapterF.HelloWorldXmpMetadata2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterF/HelloWorldReadMetadata.java
java -cp "bin;lib/iText.jar" in_action.chapterF.HelloWorldReadMetadata
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterF/HelloWorldAddMetadata.java
java -cp "bin;lib/iText.jar" in_action.chapterF.HelloWorldAddMetadata
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterF/MarkedContent.java
java -cp "bin;lib/iText.jar" in_action.chapterF.MarkedContent
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/ColumnTextExamples.java
java -cp "bin;lib/iText.jar" in_action.chapterX.ColumnTextExamples
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/Kalligraphy.java
java -cp "bin;lib/iText.jar" in_action.chapterX.Kalligraphy
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/NonBreakingSpace.java
java -cp "bin;lib/iText.jar" in_action.chapterX.NonBreakingSpace
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/NonHyphenatingHyphen.java
java -cp "bin;lib/iText.jar" in_action.chapterX.NonHyphenatingHyphen
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldForm.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/HelloWorldAddButton.java
java -cp "bin;lib/iText.jar" in_action.chapterX.HelloWorldAddButton
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/CellImageBackground.java
java -cp "bin;lib/iText.jar" in_action.chapterX.CellImageBackground
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter17/FoobarLearningAgreement.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/FoobarTranscriptOfRecordsFR.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/FoobarTranscriptOfRecords.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/FoobarTranscriptMerge.java
java -cp "bin;lib/iText.jar" in_action.chapterX.FoobarTranscriptMerge
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/HelloWorldUnicode.java
java -cp "bin;lib/iText.jar" in_action.chapterX.HelloWorldUnicode
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/GisExample.java
java -cp "bin;lib/iText.jar" in_action.chapterX.GisExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/NormalDistribution.java
java -cp "bin;lib/iText.jar" in_action.chapterX.NormalDistribution
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/RotatePages.java
java -cp "bin;lib/iText.jar" in_action.chapterX.RotatePages
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/TablesInColumns.java
java -cp "bin;lib/iText.jar" in_action.chapterX.TablesInColumns
javac -d bin -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" src/in_action/chapterX/HelloWorldEncryptedAES.java
java -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" in_action.chapterX.HelloWorldEncryptedAES
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/EmbedFontPostFacto.java
java -cp "bin;lib/iText.jar" in_action.chapterX.EmbedFontPostFacto
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapter02/HelloWorldCopy.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/HelloWorldPackage.java
java -cp "bin;lib/iText.jar" in_action.chapterX.HelloWorldPackage
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/Page.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/ReplacePagesHack.java
java -cp "bin;lib/iText.jar" in_action.chapterX.ReplacePagesHack
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/Page.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/ReplacePagesHack.java
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/ReplacePagesHack2.java
java -cp "bin;lib/iText.jar" in_action.chapterX.ReplacePagesHack2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/ReadOutLoud.java
java -cp "bin;lib/iText.jar" in_action.chapterX.ReadOutLoud
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/TooltipExample.java
java -cp "bin;lib/iText.jar" in_action.chapterX.TooltipExample
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/TooltipExample2.java
java -cp "bin;lib/iText.jar" in_action.chapterX.TooltipExample2
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/TooltipExample3.java
java -cp "bin;lib/iText.jar" in_action.chapterX.TooltipExample3
javac -d bin -cp "bin;lib/iText.jar" src/in_action/chapterX/ExtractPageLabels.java
java -cp "bin;lib/iText.jar" in_action.chapterX.ExtractPageLabels
javac -d bin -cp "bin;lib/iText.jar" src/classroom/intro/HelloWorld01.java
java -cp "bin;lib/iText.jar" classroom.intro.HelloWorld01
javac -d bin -cp "bin;lib/iText.jar" src/classroom/intro/HelloWorld02.java
java -cp "bin;lib/iText.jar" classroom.intro.HelloWorld02
javac -d bin -cp "bin;lib/iText.jar" src/classroom/intro/HelloWorld03.java
java -cp "bin;lib/iText.jar" classroom.intro.HelloWorld03
javac -d bin -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" src/classroom/intro/HelloWorld04.java
java -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" classroom.intro.HelloWorld04
javac -d bin -cp "bin;lib/iText.jar" src/classroom/intro/HelloWorld05.java
java -cp "bin;lib/iText.jar" classroom.intro.HelloWorld05
javac -d bin -cp "bin;lib/iText.jar" src/classroom/intro/HelloWorld06.java
java -cp "bin;lib/iText.jar" classroom.intro.HelloWorld06
javac -d bin -cp "bin;lib/iText.jar" src/classroom/intro/HelloWorld07.java
java -cp "bin;lib/iText.jar" classroom.intro.HelloWorld07
javac -d bin -cp "bin;lib/iText.jar" src/classroom/intro/HelloWorld08.java
java -cp "bin;lib/iText.jar" classroom.intro.HelloWorld08
javac -d bin -cp "bin;lib/iText.jar" src/classroom/intro/HelloWorld09.java
java -cp "bin;lib/iText.jar" classroom.intro.HelloWorld09
javac -d bin -cp "bin;lib/iText.jar" src/classroom/intro/HelloWorld10.java
java -cp "bin;lib/iText.jar" classroom.intro.HelloWorld10
javac -d bin -cp "bin;lib/iText.jar" src/classroom/intro/HelloWorld11.java
java -cp "bin;lib/iText.jar" classroom.intro.HelloWorld11
javac -d bin -cp "bin;lib/iText.jar" src/classroom/intro/HelloWorld12.java
java -cp "bin;lib/iText.jar" classroom.intro.HelloWorld12
javac -d bin -cp "bin;lib/iText.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar" src/classroom/filmfestival_a/Movies01.java
java -cp "bin;lib/iText.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar" classroom.filmfestival_a.Movies01
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_a/Movies02.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_a.Movies02
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_a/Movies03.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_a.Movies03
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_a/Movies04.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_a.Movies04
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_a/Movies05.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_a.Movies05
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_a/Movies06.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_a.Movies06
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_a/Movies07.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_a.Movies07
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_a/Movies08.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_a.Movies08
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_b/Movies09.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_b.Movies09
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar" src/classroom/filmfestival_b/Movies10.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar" classroom.filmfestival_b.Movies10
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar" src/classroom/filmfestival_b/Movies11.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar" classroom.filmfestival_b.Movies11
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_b/Movies12.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_b.Movies12
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_b/Movies13.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_b.Movies13
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_b/Movies14.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_b.Movies14
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_b/Movies14.java
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_b/Movies15.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_b.Movies15
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_c/AbstractMovies.java
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_c/Movies16.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_c.Movies16
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_c/AbstractMovies.java
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_c/Movies17.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_c.Movies17
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_c/Movies17.java
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_c/Movies18.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_c.Movies18
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_c/AbstractMovies.java
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_c/Movies19.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_c.Movies19
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_c/AbstractMovies.java
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_c/Movies19.java
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" src/classroom/filmfestival_c/Movies20.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/log4j-1.2.11.jar;lib/hibernate3.jar" classroom.filmfestival_c.Movies20
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar" src/classroom/filmfestival_c/Movies19.java
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar" src/classroom/filmfestival_c/Movies17.java
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar" src/classroom/filmfestival_c/AbstractMovies.java
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar" src/classroom/filmfestival_c/Movies20.java
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar" src/classroom/filmfestival_c/Movies18.java
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar" src/classroom/filmfestival_c/Movies21.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar" classroom.filmfestival_c.Movies21
javac -d bin -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar" src/classroom/filmfestival_c/Movies24.java
java -cp "bin;lib/iText.jar;lib/iText-testdatabase.jar;lib/dom4j-1.6.1.jar;lib/commons-logging-1.0.4.jar;lib/commons-collections-2.1.1.jar;lib/cglib-2.1.3.jar;lib/hsqldb.jar;lib/ehcache-1.1.jar;lib/jta.jar;lib/antlr-2.7.6rc1.jar;lib/asm-attrs.jar;lib/asm.jar;lib/hibernate3.jar;lib/log4j-1.2.11.jar" classroom.filmfestival_c.Movies24
javac -d bin -cp "bin;lib/iText.jar" src/classroom/filmfestival_c/AbstractAccreditation.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/filmfestival_c/Movies22.java
java -cp "bin;lib/iText.jar" classroom.filmfestival_c.Movies22
javac -d bin -cp "bin;lib/iText.jar" src/classroom/filmfestival_c/AbstractAccreditation.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/filmfestival_c/AccreditationData.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/filmfestival_c/Movies22.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/filmfestival_c/Movies23.java
java -cp "bin;lib/iText.jar" classroom.filmfestival_c.Movies23
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper01.java
java -cp "bin;lib/iText.jar" classroom.newspaper_a.Newspaper01
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper02.java
java -cp "bin;lib/iText.jar" classroom.newspaper_a.Newspaper02
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper03.java
java -cp "bin;lib/iText.jar" classroom.newspaper_a.Newspaper03
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper04.java
java -cp "bin;lib/iText.jar" classroom.newspaper_a.Newspaper04
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper05.java
java -cp "bin;lib/iText.jar" classroom.newspaper_a.Newspaper05
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper06.java
java -cp "bin;lib/iText.jar" classroom.newspaper_a.Newspaper06
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper07.java
java -cp "bin;lib/iText.jar" classroom.newspaper_a.Newspaper07
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_b/Newspaper08.java
java -cp "bin;lib/iText.jar" classroom.newspaper_b.Newspaper08
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_b/Newspaper09.java
java -cp "bin;lib/iText.jar" classroom.newspaper_b.Newspaper09
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_b/Newspaper10.java
java -cp "bin;lib/iText.jar" classroom.newspaper_b.Newspaper10
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_b/Newspaper11.java
java -cp "bin;lib/iText.jar" classroom.newspaper_b.Newspaper11
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_b/Newspaper11.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_b/Newspaper12.java
java -cp "bin;lib/iText.jar" classroom.newspaper_b.Newspaper12
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_b/Newspaper13.java
java -cp "bin;lib/iText.jar" classroom.newspaper_b.Newspaper13
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_a/Newspaper.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_b/Newspaper13.java
javac -d bin -cp "bin;lib/iText.jar" src/classroom/newspaper_b/Newspaper14.java
java -cp "bin;lib/iText.jar" classroom.newspaper_b.Newspaper14
javac -d bin -cp "bin;lib/iText.jar;lib/hibernate3.jar;lib/hsqldb.jar;lib/antlr-2.7.6rc1.jar;lib/asm.jar;lib/asm-attrs.jar;lib/cglib-2.1.3.jar;lib/commons-collections-2.1.1.jar;lib/commons-logging-1.0.4.jar;lib/dom4j-1.6.1.jar;lib/ehcache-1.1.jar;lib/iText-testdatabase.jar;lib/jta.jar;lib/log4j-1.2.11.jar" src/classroom/filmfestival_c/Movies25.java
java -cp "bin;lib/iText.jar;lib/hibernate3.jar;lib/hsqldb.jar;lib/antlr-2.7.6rc1.jar;lib/asm.jar;lib/asm-attrs.jar;lib/cglib-2.1.3.jar;lib/commons-collections-2.1.1.jar;lib/commons-logging-1.0.4.jar;lib/dom4j-1.6.1.jar;lib/ehcache-1.1.jar;lib/iText-testdatabase.jar;lib/jta.jar;lib/log4j-1.2.11.jar" classroom.filmfestival_c.Movies25
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/FillEnabledForm.java
java -cp "bin;lib/iText.jar" questions.forms.FillEnabledForm
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/FillEnabledFormBreakEnabling.java
java -cp "bin;lib/iText.jar" questions.forms.FillEnabledFormBreakEnabling
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/FillEnabledFormRemoveEnabling.java
java -cp "bin;lib/iText.jar" questions.forms.FillEnabledFormRemoveEnabling
javac -d bin -cp "bin;lib/iText.jar" src/questions/importpages/ExportArea.java
java -cp "bin;lib/iText.jar" questions.importpages.ExportArea
javac -d bin -cp "bin;lib/iText.jar" src/questions/separators/DottedGlue.java
java -cp "bin;lib/iText.jar" questions.separators.DottedGlue
javac -d bin -cp "bin;lib/iText.jar" src/questions/separators/PascalsTriangle.java
java -cp "bin;lib/iText.jar" questions.separators.PascalsTriangle
javac -d bin -cp "bin;lib/iText.jar" src/questions/separators/SeparatedWords1.java
java -cp "bin;lib/iText.jar" questions.separators.SeparatedWords1
javac -d bin -cp "bin;lib/iText.jar" src/questions/separators/SeparatedWords2.java
java -cp "bin;lib/iText.jar" questions.separators.SeparatedWords2
javac -d bin -cp "bin;lib/iText.jar" src/questions/separators/StarSeparators.java
java -cp "bin;lib/iText.jar" questions.separators.StarSeparators
javac -d bin -cp "bin;lib/iText.jar" src/questions/separators/LineSeparator1.java
java -cp "bin;lib/iText.jar" questions.separators.LineSeparator1
javac -d bin -cp "bin;lib/iText.jar" src/questions/separators/LineSeparator2.java
java -cp "bin;lib/iText.jar" questions.separators.LineSeparator2
javac -d bin -cp "bin;lib/iText.jar" src/questions/separators/LineSeparator3.java
java -cp "bin;lib/iText.jar" questions.separators.LineSeparator3
javac -d bin -cp "bin;lib/iText.jar" src/questions/separators/PositionedMarks.java
java -cp "bin;lib/iText.jar" questions.separators.PositionedMarks
javac -d bin -cp "bin;lib/iText.jar" src/questions/separators/TabbedWords1.java
java -cp "bin;lib/iText.jar" questions.separators.TabbedWords1
javac -d bin -cp "bin;lib/iText.jar" src/questions/separators/TabbedWords2.java
java -cp "bin;lib/iText.jar" questions.separators.TabbedWords2
javac -d bin -cp "bin;lib/iText.jar" src/questions/separators/TOCExample.java
java -cp "bin;lib/iText.jar" questions.separators.TOCExample
javac -d bin -cp "bin;lib/iText.jar" src/questions/images/PostCard.java
java -cp "bin;lib/iText.jar" questions.images.PostCard
javac -d bin -cp "bin;lib/iText.jar" src/questions/importpages/HelloWorldImportedPages.java
java -cp "bin;lib/iText.jar" questions.importpages.HelloWorldImportedPages
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/GetTextFields.java
java -cp "bin;lib/iText.jar" questions.forms.GetTextFields
javac -d bin -cp "bin;lib/iText.jar" src/questions/graphics2D/SplitCanvas.java
java -cp "bin;lib/iText.jar" questions.graphics2D.SplitCanvas
javac -d bin -cp "bin;lib/iText.jar" src/questions/graphics2D/SplitCanvasDifficult.java
java -cp "bin;lib/iText.jar" questions.graphics2D.SplitCanvasDifficult
javac -d bin -cp "bin;lib/iText.jar" src/questions/graphics2D/SplitCanvasEasy.java
java -cp "bin;lib/iText.jar" questions.graphics2D.SplitCanvasEasy
javac -d bin -cp "bin;lib/iText.jar" src/questions/tables/RotateCell.java
java -cp "bin;lib/iText.jar" questions.tables.RotateCell
javac -d bin -cp "bin;lib/iText.jar" src/questions/graphics2D/SwingForceArialUni.java
java -cp "bin;lib/iText.jar" questions.graphics2D.SwingForceArialUni
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/AddActionToField.java
java -cp "bin;lib/iText.jar" questions.forms.AddActionToField
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/KidsOnDifferentPages.java
java -cp "bin;lib/iText.jar" questions.forms.KidsOnDifferentPages
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/RadioButtonsOnDifferentPages.java
java -cp "bin;lib/iText.jar" questions.forms.RadioButtonsOnDifferentPages
javac -d bin -cp "bin;lib/iText.jar" src/questions/fonts/EncodingFont.java
java -cp "bin;lib/iText.jar" questions.fonts.EncodingFont
javac -d bin -cp "bin;lib/iText.jar" src/questions/objects/MemoryTests.java
java -cp "bin;lib/iText.jar" questions.objects.MemoryTests
javac -d bin -cp "bin;lib/iText.jar" src/questions/objects/NestedList.java
java -cp "bin;lib/iText.jar" questions.objects.NestedList
javac -d bin -cp "bin;lib/iText.jar" src/questions/metadata/ReplaceXMP.java
java -cp "bin;lib/iText.jar" questions.metadata.ReplaceXMP
javac -d bin -cp "bin;lib/iText.jar" src/questions/stamppages/PageXofYRightAligned.java
java -cp "bin;lib/iText.jar" questions.stamppages.PageXofYRightAligned
javac -d bin -cp "bin;lib/iText.jar" src/questions/stamppages/ParagraphBookmarkEvents.java
javac -d bin -cp "bin;lib/iText.jar" src/questions/stamppages/BookmarksToTOC1.java
java -cp "bin;lib/iText.jar" questions.stamppages.BookmarksToTOC1
javac -d bin -cp "bin;lib/iText.jar" src/questions/stamppages/ParagraphBookmarkEvents.java
javac -d bin -cp "bin;lib/iText.jar" src/questions/stamppages/BookmarksToTOC2.java
java -cp "bin;lib/iText.jar" questions.stamppages.BookmarksToTOC2
javac -d bin -cp "bin;lib/iText.jar" src/questions/importpages/ConcatenateWithTOC.java
java -cp "bin;lib/iText.jar" questions.importpages.ConcatenateWithTOC
javac -d bin -cp "bin;lib/iText.jar" src/questions/importpages/ConcatenateWithTOC2.java
java -cp "bin;lib/iText.jar" questions.importpages.ConcatenateWithTOC2
javac -d bin -cp "bin;lib/iText.jar" src/questions/tables/AlternateBackground.java
javac -d bin -cp "bin;lib/iText.jar" src/questions/tables/TableHeaderAlternateBackground.java
java -cp "bin;lib/iText.jar" questions.tables.TableHeaderAlternateBackground
javac -d bin -cp "bin;lib/iText.jar" src/questions/javascript/VersionChecker.java
java -cp "bin;lib/iText.jar" questions.javascript.VersionChecker
javac -d bin -cp "bin;lib/iText.jar" src/questions/javascript/TriggerMenuButtons.java
java -cp "bin;lib/iText.jar" questions.javascript.TriggerMenuButtons
javac -d bin -cp "bin;lib/iText.jar" src/questions/tables/AutomaticExtensionOfTables.java
java -cp "bin;lib/iText.jar" questions.tables.AutomaticExtensionOfTables
javac -d bin -cp "bin;lib/iText.jar" src/questions/images/OverlappingImages.java
java -cp "bin;lib/iText.jar" questions.images.OverlappingImages
javac -d bin -cp "bin;lib/iText.jar" src/questions/ocg/LockedLayers.java
java -cp "bin;lib/iText.jar" questions.ocg.LockedLayers
javac -d bin -cp "bin;lib/iText.jar" src/questions/directcontent/InterpretOCR.java
java -cp "bin;lib/iText.jar" questions.directcontent.InterpretOCR
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/RemoveXfa.java
java -cp "bin;lib/iText.jar" questions.forms.RemoveXfa
javac -d bin -cp "bin;lib/iText.jar" src/questions/importpages/ConcatenateMakeTOC.java
java -cp "bin;lib/iText.jar" questions.importpages.ConcatenateMakeTOC
javac -d bin -cp "bin;lib/iText.jar" src/questions/images/PostCardExtra.java
java -cp "bin;lib/iText.jar" questions.images.PostCardExtra
javac -d bin -cp "bin;lib/iText.jar" src/questions/objects/NewPageColumns.java
java -cp "bin;lib/iText.jar" questions.objects.NewPageColumns
javac -d bin -cp "bin;lib/iText.jar" src/questions/tables/AddTableAsHeaderFooter.java
java -cp "bin;lib/iText.jar" questions.tables.AddTableAsHeaderFooter
javac -d bin -cp "bin;lib/iText.jar" src/questions/tables/TableColumns.java
java -cp "bin;lib/iText.jar" questions.tables.TableColumns
javac -d bin -cp "bin;lib/iText.jar" src/questions/objects/DifferentLeadings.java
java -cp "bin;lib/iText.jar" questions.objects.DifferentLeadings
javac -d bin -cp "bin;lib/iText.jar" src/questions/ocg/StatusBars1.java
java -cp "bin;lib/iText.jar" questions.ocg.StatusBars1
javac -d bin -cp "bin;lib/iText.jar" src/questions/ocg/StatusBars2.java
java -cp "bin;lib/iText.jar" questions.ocg.StatusBars2
javac -d bin -cp "bin;lib/iText.jar" src/questions/tables/TablesWriteSelected.java
java -cp "bin;lib/iText.jar" questions.tables.TablesWriteSelected
javac -d bin -cp "bin;lib/iText.jar" src/questions/tables/RotatedCells.java
java -cp "bin;lib/iText.jar" questions.tables.RotatedCells
javac -d bin -cp "bin;lib/iText.jar" src/questions/graphics2D/ArabicText.java
java -cp "bin;lib/iText.jar" questions.graphics2D.ArabicText
javac -d bin -cp "bin;lib/iText.jar" src/questions/compression/CompressionLevelsWriter.java
java -cp "bin;lib/iText.jar" questions.compression.CompressionLevelsWriter
javac -d bin -cp "bin;lib/iText.jar" src/questions/compression/CompressionLevelsImage.java
java -cp "bin;lib/iText.jar" questions.compression.CompressionLevelsImage
javac -d bin -cp "bin;lib/iText.jar" src/questions/compression/CompressionLevelsFonts.java
java -cp "bin;lib/iText.jar" questions.compression.CompressionLevelsFonts
javac -d bin -cp "bin;lib/iText.jar" src/questions/compression/CompressionLevelsEmbeddedFiles.java
java -cp "bin;lib/iText.jar" questions.compression.CompressionLevelsEmbeddedFiles
javac -d bin -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" src/questions/encryption/HelloWorldFullyEncrypted.java
java -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" questions.encryption.HelloWorldFullyEncrypted
javac -d bin -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" src/questions/encryption/HelloWorldMetadataNotEncrypted.java
java -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" questions.encryption.HelloWorldMetadataNotEncrypted
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/MultipleChoice.java
java -cp "bin;lib/iText.jar" questions.forms.MultipleChoice
javac -d bin -cp "bin;lib/iText.jar" src/questions/metadata/UpdateModDate.java
java -cp "bin;lib/iText.jar" questions.metadata.UpdateModDate
javac -d bin -cp "bin;lib/iText.jar" src/questions/images/MakingImageTransparent.java
java -cp "bin;lib/iText.jar" questions.images.MakingImageTransparent
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/AddFieldToExistingForm.java
java -cp "bin;lib/iText.jar" questions.forms.AddFieldToExistingForm
javac -d bin -cp "bin;lib/iText.jar" src/questions/images/TransparentEllipse1.java
java -cp "bin;lib/iText.jar" questions.images.TransparentEllipse1
javac -d bin -cp "bin;lib/iText.jar" src/questions/images/TransparentEllipse2.java
java -cp "bin;lib/iText.jar" questions.images.TransparentEllipse2
javac -d bin -cp "bin;lib/iText.jar" src/questions/stamppages/AddCropbox.java
java -cp "bin;lib/iText.jar" questions.stamppages.AddCropbox
javac -d bin -cp "bin;lib/iText.jar;lib/iText-rtf.jar" src/rtf/intro/RTFHelloWorld1.java
java -cp "bin;lib/iText.jar;lib/iText-rtf.jar" rtf.intro.RTFHelloWorld1
javac -d bin -cp "bin;lib/iText.jar" src/questions/tables/TableAndHTMLWorker.java
java -cp "bin;lib/iText.jar" questions.tables.TableAndHTMLWorker
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/MultipleChoice2.java
java -cp "bin;lib/iText.jar" questions.forms.MultipleChoice2
javac -d bin -cp "bin;lib/iText.jar" src/questions/importpages/NameCard.java
java -cp "bin;lib/iText.jar" questions.importpages.NameCard
javac -d bin -cp "bin;lib/iText.jar" src/questions/importpages/NameCard.java
javac -d bin -cp "bin;lib/iText.jar" src/questions/importpages/NameCards.java
java -cp "bin;lib/iText.jar" questions.importpages.NameCards
javac -d bin -cp "bin;lib/iText.jar" src/questions/images/ResizeImage.java
java -cp "bin;lib/iText.jar" questions.images.ResizeImage
javac -d bin -cp "bin;lib/iText.jar" src/questions/objects/ChaptersAndMemory.java
java -cp "bin;lib/iText.jar" questions.objects.ChaptersAndMemory
javac -d bin -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" src/questions/stamppages/CertificationSig.java
java -cp "bin;lib/iText.jar;lib/bcmail-jdk14-138.jar;lib/bcprov-jdk14-138.jar" questions.stamppages.CertificationSig
javac -d bin -cp "bin;lib/iText.jar" src/questions/directcontent/Logo.java
java -cp "bin;lib/iText.jar" questions.directcontent.Logo
javac -d bin -cp "bin;lib/iText.jar" src/questions/compression/ZipPdfFiles.java
java -cp "bin;lib/iText.jar" questions.compression.ZipPdfFiles
javac -d bin -cp "bin;lib/iText.jar;lib/iText-rtf.jar" src/rtf/shapes/RTFShapeTextWrap.java
java -cp "bin;lib/iText.jar;lib/iText-rtf.jar" rtf.shapes.RTFShapeTextWrap
javac -d bin -cp "bin;lib/iText.jar" src/questions/ocg/AddOptionalWatermark.java
java -cp "bin;lib/iText.jar" questions.ocg.AddOptionalWatermark
javac -d bin -cp "bin;lib/iText.jar" src/questions/markedcontent/ObjectData.java
java -cp "bin;lib/iText.jar" questions.markedcontent.ObjectData
javac -d bin -cp "bin;lib/iText.jar" src/questions/stamppages/IncreaseMediabox.java
java -cp "bin;lib/iText.jar" questions.stamppages.IncreaseMediabox
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/ReadXfa.java
java -cp "bin;lib/iText.jar" questions.forms.ReadXfa
javac -d bin -cp "bin;lib/iText.jar" src/questions/stamppages/ChangeViewerPreferences.java
java -cp "bin;lib/iText.jar" questions.stamppages.ChangeViewerPreferences
javac -d bin -cp "bin;lib/iText.jar" src/questions/stamppages/RemoveAttachmentAnnotations.java
java -cp "bin;lib/iText.jar" questions.stamppages.RemoveAttachmentAnnotations
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/FillDynamicXfa.java
java -cp "bin;lib/iText.jar" questions.forms.FillDynamicXfa
javac -d bin -cp "bin;lib/iText.jar" src/questions/images/TransparentPng.java
java -cp "bin;lib/iText.jar" questions.images.TransparentPng
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/FormWithTooltips.java
java -cp "bin;lib/iText.jar" questions.forms.FormWithTooltips
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/ChangeTextFieldAlignment.java
java -cp "bin;lib/iText.jar" questions.forms.ChangeTextFieldAlignment
javac -d bin -cp "bin;lib/iText.jar" src/questions/objects/NestingLists.java
java -cp "bin;lib/iText.jar" questions.objects.NestingLists
javac -d bin -cp "bin;lib/iText.jar" src/questions/javascript/AddJavaScriptToForm.java
java -cp "bin;lib/iText.jar" questions.javascript.AddJavaScriptToForm
javac -d bin -cp "bin;lib/iText.jar" src/questions/images/CustomizedTitleBar.java
java -cp "bin;lib/iText.jar" questions.images.CustomizedTitleBar
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/ReadXfa2.java
java -cp "bin;lib/iText.jar" questions.forms.ReadXfa2
javac -d bin -cp "bin;lib/iText.jar" src/questions/forms/FillDynamicXfa2.java
java -cp "bin;lib/iText.jar" questions.forms.FillDynamicXfa2
