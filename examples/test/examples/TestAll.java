package examples;

import org.junit.Test;

public class TestAll {

  private static final String ARGS[] = {};

  @Test
  public void test_classroom_filmfestival_a_Movies01() throws Exception {
    classroom.filmfestival_a.Movies01.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_a_Movies02() throws Exception {
    classroom.filmfestival_a.Movies02.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_a_Movies03() throws Exception {
    classroom.filmfestival_a.Movies03.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_a_Movies04() throws Exception {
    classroom.filmfestival_a.Movies04.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_a_Movies05() throws Exception {
    classroom.filmfestival_a.Movies05.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_a_Movies06() throws Exception {
    classroom.filmfestival_a.Movies06.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_a_Movies07() throws Exception {
    classroom.filmfestival_a.Movies07.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_a_Movies08() throws Exception {
    classroom.filmfestival_a.Movies08.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_b_Movies09() throws Exception {
    classroom.filmfestival_b.Movies09.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_b_Movies10() throws Exception {
    classroom.filmfestival_b.Movies10.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_b_Movies11() throws Exception {
    classroom.filmfestival_b.Movies11.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_b_Movies12() throws Exception {
    classroom.filmfestival_b.Movies12.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_b_Movies13() throws Exception {
    classroom.filmfestival_b.Movies13.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_b_Movies14() throws Exception {
    classroom.filmfestival_b.Movies14.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_b_Movies15() throws Exception {
    classroom.filmfestival_b.Movies15.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_c_Movies16() throws Exception {
    classroom.filmfestival_c.Movies16.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_c_Movies17() throws Exception {
    classroom.filmfestival_c.Movies17.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_c_Movies18() throws Exception {
    classroom.filmfestival_c.Movies18.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_c_Movies19() throws Exception {
    classroom.filmfestival_c.Movies19.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_c_Movies20() throws Exception {
    classroom.filmfestival_c.Movies20.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_c_Movies21() throws Exception {
    classroom.filmfestival_c.Movies21.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_c_Movies22() throws Exception {
    classroom.filmfestival_c.Movies22.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_c_Movies23() throws Exception {
    classroom.filmfestival_c.Movies23.main(ARGS);
  }

  @Test
  public void test_classroom_filmfestival_c_Movies24() throws Exception {
    classroom.filmfestival_c.Movies24.main(ARGS);
  }

  @Test
  public void test_classroom_intro_HelloWorld01() throws Exception {
    classroom.intro.HelloWorld01.main(ARGS);
  }

  @Test
  public void test_classroom_intro_HelloWorld02() throws Exception {
    classroom.intro.HelloWorld02.main(ARGS);
  }

  @Test
  public void test_classroom_intro_HelloWorld03() throws Exception {
    classroom.intro.HelloWorld03.main(ARGS);
  }

  @Test
  public void test_classroom_intro_HelloWorld04() throws Exception {
    classroom.intro.HelloWorld04.main(ARGS);
  }

  @Test
  public void test_classroom_intro_HelloWorld05() throws Exception {
    classroom.intro.HelloWorld05.main(ARGS);
  }

  @Test
  public void test_classroom_intro_HelloWorld06() throws Exception {
    classroom.intro.HelloWorld06.main(ARGS);
  }

  @Test
  public void test_classroom_intro_HelloWorld07() throws Exception {
    classroom.intro.HelloWorld07.main(ARGS);
  }

  @Test
  public void test_classroom_intro_HelloWorld08() throws Exception {
    classroom.intro.HelloWorld08.main(ARGS);
  }

  @Test
  public void test_classroom_intro_HelloWorld09() throws Exception {
    classroom.intro.HelloWorld09.main(ARGS);
  }

  @Test
  public void test_classroom_intro_HelloWorld10() throws Exception {
    classroom.intro.HelloWorld10.main(ARGS);
  }

  @Test
  public void test_classroom_intro_HelloWorld11() throws Exception {
    classroom.intro.HelloWorld11.main(ARGS);
  }

  @Test
  public void test_classroom_intro_HelloWorld12() throws Exception {
    classroom.intro.HelloWorld12.main(ARGS);
  }

  @Test
  public void test_classroom_newspaper_a_Newspaper01() throws Exception {
    classroom.newspaper_a.Newspaper01.main(ARGS);
  }

  @Test
  public void test_classroom_newspaper_a_Newspaper02() throws Exception {
    classroom.newspaper_a.Newspaper02.main(ARGS);
  }

  @Test
  public void test_classroom_newspaper_a_Newspaper03() throws Exception {
    classroom.newspaper_a.Newspaper03.main(ARGS);
  }

  @Test
  public void test_classroom_newspaper_a_Newspaper04() throws Exception {
    classroom.newspaper_a.Newspaper04.main(ARGS);
  }

  @Test
  public void test_classroom_newspaper_a_Newspaper05() throws Exception {
    classroom.newspaper_a.Newspaper05.main(ARGS);
  }

  @Test
  public void test_classroom_newspaper_a_Newspaper06() throws Exception {
    classroom.newspaper_a.Newspaper06.main(ARGS);
  }

  @Test
  public void test_classroom_newspaper_a_Newspaper07() throws Exception {
    classroom.newspaper_a.Newspaper07.main(ARGS);
  }

  @Test
  public void test_classroom_newspaper_b_Newspaper08() throws Exception {
    classroom.newspaper_b.Newspaper08.main(ARGS);
  }

  @Test
  public void test_classroom_newspaper_b_Newspaper09() throws Exception {
    classroom.newspaper_b.Newspaper09.main(ARGS);
  }

  @Test
  public void test_classroom_newspaper_b_Newspaper10() throws Exception {
    classroom.newspaper_b.Newspaper10.main(ARGS);
  }

  @Test
  public void test_classroom_newspaper_b_Newspaper11() throws Exception {
    classroom.newspaper_b.Newspaper11.main(ARGS);
  }

  @Test
  public void test_classroom_newspaper_b_Newspaper12() throws Exception {
    classroom.newspaper_b.Newspaper12.main(ARGS);
  }

  @Test
  public void test_classroom_newspaper_b_Newspaper13() throws Exception {
    classroom.newspaper_b.Newspaper13.main(ARGS);
  }

  @Test
  public void test_classroom_newspaper_b_Newspaper14() throws Exception {
    classroom.newspaper_b.Newspaper14.main(ARGS);
  }

  @Test
  public void test_in_action_chapter01_HelloWorld() throws Exception {
    in_action.chapter01.HelloWorld.main(ARGS);
  }

  @Test
  public void test_in_action_chapter01_HelloWorldBurst() throws Exception {
    in_action.chapter01.HelloWorldBurst.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorld() throws Exception {
    in_action.chapter02.HelloWorld.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldAbsolute() throws Exception {
    in_action.chapter02.HelloWorldAbsolute.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldAddMetadata() throws Exception {
    in_action.chapter02.HelloWorldAddMetadata.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldBlue() throws Exception {
    in_action.chapter02.HelloWorldBlue.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldBookmarks() throws Exception {
    in_action.chapter02.HelloWorldBookmarks.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldCopy() throws Exception {
    in_action.chapter02.HelloWorldCopy.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldCopyFields() throws Exception {
    in_action.chapter02.HelloWorldCopyFields.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldCopyForm() throws Exception {
    in_action.chapter02.HelloWorldCopyForm.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldCopyStamp() throws Exception {
    in_action.chapter02.HelloWorldCopyStamp.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldForm() throws Exception {
    in_action.chapter02.HelloWorldForm.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldGraphics2D() throws Exception {
    in_action.chapter02.HelloWorldGraphics2D.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldImportedPages() throws Exception {
    in_action.chapter02.HelloWorldImportedPages.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldLandscape() throws Exception {
    in_action.chapter02.HelloWorldLandscape.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldLandscape2() throws Exception {
    in_action.chapter02.HelloWorldLandscape2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldLetter() throws Exception {
    in_action.chapter02.HelloWorldLetter.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldMargins() throws Exception {
    in_action.chapter02.HelloWorldMargins.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldMetadata() throws Exception {
    in_action.chapter02.HelloWorldMetadata.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldMirroredMargins() throws Exception {
    in_action.chapter02.HelloWorldMirroredMargins.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldMultiple() throws Exception {
    in_action.chapter02.HelloWorldMultiple.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldNarrow() throws Exception {
    in_action.chapter02.HelloWorldNarrow.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldOpen() throws Exception {
    in_action.chapter02.HelloWorldOpen.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldPartialReader() throws Exception {
    in_action.chapter02.HelloWorldPartialReader.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldReader() throws Exception {
    in_action.chapter02.HelloWorldReader.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldReadMetadata() throws Exception {
    in_action.chapter02.HelloWorldReadMetadata.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldSelectedPages() throws Exception {
    in_action.chapter02.HelloWorldSelectedPages.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldSelectPages() throws Exception {
    in_action.chapter02.HelloWorldSelectPages.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldStampCopy() throws Exception {
    in_action.chapter02.HelloWorldStampCopy.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldStampCopyStamp() throws Exception {
    in_action.chapter02.HelloWorldStampCopyStamp.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldStamper() throws Exception {
    in_action.chapter02.HelloWorldStamper.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldStamper2() throws Exception {
    in_action.chapter02.HelloWorldStamper2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldStamperAdvanced() throws Exception {
    in_action.chapter02.HelloWorldStamperAdvanced.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldStamperImportedPages() throws Exception {
    in_action.chapter02.HelloWorldStamperImportedPages.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldSystemOut() throws Exception {
    in_action.chapter02.HelloWorldSystemOut.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldVersion_1_6() throws Exception {
    in_action.chapter02.HelloWorldVersion_1_6.main(ARGS);
  }

  @Test
  public void test_in_action_chapter02_HelloWorldWriter() throws Exception {
    in_action.chapter02.HelloWorldWriter.main(ARGS);
  }

  @Test
  public void test_in_action_chapter03_HelloWorldCompression() throws Exception {
    in_action.chapter03.HelloWorldCompression.main(ARGS);
  }

  @Test
  public void test_in_action_chapter03_HelloWorldEncryptDecrypt() throws Exception {
    in_action.chapter03.HelloWorldEncryptDecrypt.main(ARGS);
  }

  @Test
  public void test_in_action_chapter03_HelloWorldEncrypted() throws Exception {
    in_action.chapter03.HelloWorldEncrypted.main(ARGS);
  }

  @Test
  public void test_in_action_chapter03_HelloWorldFullyCompressed() throws Exception {
    in_action.chapter03.HelloWorldFullyCompressed.main(ARGS);
  }

  @Test
  public void test_in_action_chapter03_HelloWorldMaximum() throws Exception {
    in_action.chapter03.HelloWorldMaximum.main(ARGS);
  }

  @Test
  public void test_in_action_chapter03_HelloWorldUncompressed() throws Exception {
    in_action.chapter03.HelloWorldUncompressed.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_DickensHyphenated() throws Exception {
    in_action.chapter04.DickensHyphenated.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoobarFlyer() throws Exception {
    in_action.chapter04.FoobarFlyer.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogAnchor1() throws Exception {
    in_action.chapter04.FoxDogAnchor1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogAnchor2() throws Exception {
    in_action.chapter04.FoxDogAnchor2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogChapter1() throws Exception {
    in_action.chapter04.FoxDogChapter1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogChapter2() throws Exception {
    in_action.chapter04.FoxDogChapter2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogChunk1() throws Exception {
    in_action.chapter04.FoxDogChunk1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogChunk2() throws Exception {
    in_action.chapter04.FoxDogChunk2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogColor() throws Exception {
    in_action.chapter04.FoxDogColor.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogGeneric1() throws Exception {
    in_action.chapter04.FoxDogGeneric1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogGeneric2() throws Exception {
    in_action.chapter04.FoxDogGeneric2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogGeneric3() throws Exception {
    in_action.chapter04.FoxDogGeneric3.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogGoto1() throws Exception {
    in_action.chapter04.FoxDogGoto1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogGoto2() throws Exception {
    in_action.chapter04.FoxDogGoto2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogGoto3() throws Exception {
    in_action.chapter04.FoxDogGoto3.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogGoto4() throws Exception {
    in_action.chapter04.FoxDogGoto4.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogList1() throws Exception {
    in_action.chapter04.FoxDogList1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogList2() throws Exception {
    in_action.chapter04.FoxDogList2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogParagraph() throws Exception {
    in_action.chapter04.FoxDogParagraph.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogPhrase() throws Exception {
    in_action.chapter04.FoxDogPhrase.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogRender() throws Exception {
    in_action.chapter04.FoxDogRender.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogScale() throws Exception {
    in_action.chapter04.FoxDogScale.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogSkew() throws Exception {
    in_action.chapter04.FoxDogSkew.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogSpaceCharRatio() throws Exception {
    in_action.chapter04.FoxDogSpaceCharRatio.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogSplit() throws Exception {
    in_action.chapter04.FoxDogSplit.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogSupSubscript() throws Exception {
    in_action.chapter04.FoxDogSupSubscript.main(ARGS);
  }

  @Test
  public void test_in_action_chapter04_FoxDogUnderline() throws Exception {
    in_action.chapter04.FoxDogUnderline.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_Barcodes() throws Exception {
    in_action.chapter05.Barcodes.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoobarFlyer() throws Exception {
    in_action.chapter05.FoobarFlyer.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoxDogAnimatedGif() throws Exception {
    in_action.chapter05.FoxDogAnimatedGif.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoxDogImageAlignment() throws Exception {
    in_action.chapter05.FoxDogImageAlignment.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoxDogImageChunk() throws Exception {
    in_action.chapter05.FoxDogImageChunk.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoxDogImageMask() throws Exception {
    in_action.chapter05.FoxDogImageMask.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoxDogImageRectangle() throws Exception {
    in_action.chapter05.FoxDogImageRectangle.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoxDogImageRotation() throws Exception {
    in_action.chapter05.FoxDogImageRotation.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoxDogImageScaling1() throws Exception {
    in_action.chapter05.FoxDogImageScaling1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoxDogImageScaling2() throws Exception {
    in_action.chapter05.FoxDogImageScaling2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoxDogImageSequence() throws Exception {
    in_action.chapter05.FoxDogImageSequence.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoxDogImageTranslation() throws Exception {
    in_action.chapter05.FoxDogImageTranslation.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoxDogImageTypes() throws Exception {
    in_action.chapter05.FoxDogImageTypes.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoxDogImageWrapping() throws Exception {
    in_action.chapter05.FoxDogImageWrapping.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoxDogMultipageTiff() throws Exception {
    in_action.chapter05.FoxDogMultipageTiff.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_FoxDogRawImage() throws Exception {
    in_action.chapter05.FoxDogRawImage.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_HitchcockAwt() throws Exception {
    in_action.chapter05.HitchcockAwt.main(ARGS);
  }

  @Test
  public void test_in_action_chapter05_HitchcockAwtImage() throws Exception {
    in_action.chapter05.HitchcockAwtImage.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_FoobarStudyProgram() throws Exception {
    in_action.chapter06.FoobarStudyProgram.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_MyFirstPdfPTable() throws Exception {
    in_action.chapter06.MyFirstPdfPTable.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_MyFirstTable() throws Exception {
    in_action.chapter06.MyFirstTable.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableAbsoluteColumns() throws Exception {
    in_action.chapter06.PdfPTableAbsoluteColumns.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableAbsolutePositions() throws Exception {
    in_action.chapter06.PdfPTableAbsolutePositions.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableAbsoluteWidth() throws Exception {
    in_action.chapter06.PdfPTableAbsoluteWidth.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableAbsoluteWidths() throws Exception {
    in_action.chapter06.PdfPTableAbsoluteWidths.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableAligned() throws Exception {
    in_action.chapter06.PdfPTableAligned.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableCellAlignment() throws Exception {
    in_action.chapter06.PdfPTableCellAlignment.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableCellHeights() throws Exception {
    in_action.chapter06.PdfPTableCellHeights.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableCellSpacing() throws Exception {
    in_action.chapter06.PdfPTableCellSpacing.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableColors() throws Exception {
    in_action.chapter06.PdfPTableColors.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableColumnWidths() throws Exception {
    in_action.chapter06.PdfPTableColumnWidths.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableCompare() throws Exception {
    in_action.chapter06.PdfPTableCompare.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableImages() throws Exception {
    in_action.chapter06.PdfPTableImages.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableMemoryFriendly() throws Exception {
    in_action.chapter06.PdfPTableMemoryFriendly.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableNested() throws Exception {
    in_action.chapter06.PdfPTableNested.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableRepeatHeader() throws Exception {
    in_action.chapter06.PdfPTableRepeatHeader.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableRepeatHeaderFooter() throws Exception {
    in_action.chapter06.PdfPTableRepeatHeaderFooter.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableSpacing() throws Exception {
    in_action.chapter06.PdfPTableSpacing.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableSplit() throws Exception {
    in_action.chapter06.PdfPTableSplit.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableSplitVertically() throws Exception {
    in_action.chapter06.PdfPTableSplitVertically.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableVerticalCells() throws Exception {
    in_action.chapter06.PdfPTableVerticalCells.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_PdfPTableWithoutBorders() throws Exception {
    in_action.chapter06.PdfPTableWithoutBorders.main(ARGS);
  }

  @Test
  public void test_in_action_chapter06_SpecificCells() throws Exception {
    in_action.chapter06.SpecificCells.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_ColumnControl() throws Exception {
    in_action.chapter07.ColumnControl.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_ColumnElements() throws Exception {
    in_action.chapter07.ColumnElements.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_ColumnProperties() throws Exception {
    in_action.chapter07.ColumnProperties.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_ColumnsIrregular() throws Exception {
    in_action.chapter07.ColumnsIrregular.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_ColumnsRegular() throws Exception {
    in_action.chapter07.ColumnsRegular.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_ColumnWithAddElement() throws Exception {
    in_action.chapter07.ColumnWithAddElement.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_ColumnWithAddText() throws Exception {
    in_action.chapter07.ColumnWithAddText.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_ColumnWithSetSimpleColumn() throws Exception {
    in_action.chapter07.ColumnWithSetSimpleColumn.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_ColumnWithSetText() throws Exception {
    in_action.chapter07.ColumnWithSetText.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_FoobarCourseCatalog() throws Exception {
    in_action.chapter07.FoobarCourseCatalog.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_MultiColumnIrregular() throws Exception {
    in_action.chapter07.MultiColumnIrregular.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_MultiColumnPoem() throws Exception {
    in_action.chapter07.MultiColumnPoem.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_MultiColumnPoemCustom() throws Exception {
    in_action.chapter07.MultiColumnPoemCustom.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_MultiColumnPoemReverse() throws Exception {
    in_action.chapter07.MultiColumnPoemReverse.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_ParagraphPositions() throws Exception {
    in_action.chapter07.ParagraphPositions.main(ARGS);
  }

  @Test
  public void test_in_action_chapter07_ParagraphText() throws Exception {
    in_action.chapter07.ParagraphText.main(ARGS);
  }

  @Test
  public void test_in_action_chapter08_ChineseKoreanJapaneseFonts() throws Exception {
    in_action.chapter08.ChineseKoreanJapaneseFonts.main(ARGS);
  }

  @Test
  public void test_in_action_chapter08_CIDTrueTypeOutlines() throws Exception {
    in_action.chapter08.CIDTrueTypeOutlines.main(ARGS);
  }

  @Test
  public void test_in_action_chapter08_CompactFontFormatExample() throws Exception {
    in_action.chapter08.CompactFontFormatExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapter08_FileSizeComparison() throws Exception {
    in_action.chapter08.FileSizeComparison.main(ARGS);
  }

  @Test
  public void test_in_action_chapter08_FontMetrics() throws Exception {
    in_action.chapter08.FontMetrics.main(ARGS);
  }

  @Test
  public void test_in_action_chapter08_StandardType1FontFromAFM() throws Exception {
    in_action.chapter08.StandardType1FontFromAFM.main(ARGS);
  }

  @Test
  public void test_in_action_chapter08_StandardType1Fonts() throws Exception {
    in_action.chapter08.StandardType1Fonts.main(ARGS);
  }

  @Test
  public void test_in_action_chapter08_TrueTypeCollections() throws Exception {
    in_action.chapter08.TrueTypeCollections.main(ARGS);
  }

  @Test
  public void test_in_action_chapter08_TrueTypeFontEncoding() throws Exception {
    in_action.chapter08.TrueTypeFontEncoding.main(ARGS);
  }

  @Test
  public void test_in_action_chapter08_TrueTypeFontExample() throws Exception {
    in_action.chapter08.TrueTypeFontExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapter08_Type1FontFromAFM() throws Exception {
    in_action.chapter08.Type1FontFromAFM.main(ARGS);
  }

  @Test
  public void test_in_action_chapter08_Type1FontFromPFBwithAFM() throws Exception {
    in_action.chapter08.Type1FontFromPFBwithAFM.main(ARGS);
  }

  @Test
  public void test_in_action_chapter08_Type1FontFromPFBwithPFM() throws Exception {
    in_action.chapter08.Type1FontFromPFBwithPFM.main(ARGS);
  }

  @Test
  public void test_in_action_chapter08_Type3Characters() throws Exception {
    in_action.chapter08.Type3Characters.main(ARGS);
  }

  @Test
  public void test_in_action_chapter09_Diacritics1() throws Exception {
    in_action.chapter09.Diacritics1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter09_Diacritics2() throws Exception {
    in_action.chapter09.Diacritics2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter09_FontFactoryExample1() throws Exception {
    in_action.chapter09.FontFactoryExample1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter09_FontFactoryExample2() throws Exception {
    in_action.chapter09.FontFactoryExample2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter09_FontSelectionExample() throws Exception {
    in_action.chapter09.FontSelectionExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapter09_Ligatures1() throws Exception {
    in_action.chapter09.Ligatures1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter09_Ligatures2() throws Exception {
    in_action.chapter09.Ligatures2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter09_Monospace() throws Exception {
    in_action.chapter09.Monospace.main(ARGS);
  }

  @Test
  public void test_in_action_chapter09_Peace() throws Exception {
    in_action.chapter09.Peace.main(ARGS);
  }

  @Test
  public void test_in_action_chapter09_RightToLeftExample() throws Exception {
    in_action.chapter09.RightToLeftExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapter09_SayPeace() throws Exception {
    in_action.chapter09.SayPeace.main(ARGS);
  }

  @Test
  public void test_in_action_chapter09_SymbolSubstitution() throws Exception {
    in_action.chapter09.SymbolSubstitution.main(ARGS);
  }

  @Test
  public void test_in_action_chapter09_VerticalTextExample() throws Exception {
    in_action.chapter09.VerticalTextExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_ConstructingPaths1() throws Exception {
    in_action.chapter10.ConstructingPaths1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_ConstructingPaths2() throws Exception {
    in_action.chapter10.ConstructingPaths2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_ConstructingPaths3() throws Exception {
    in_action.chapter10.ConstructingPaths3.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_ConstructingPaths4() throws Exception {
    in_action.chapter10.ConstructingPaths4.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_DirectContent() throws Exception {
    in_action.chapter10.DirectContent.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_EyeCoordinates() throws Exception {
    in_action.chapter10.EyeCoordinates.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_EyeImages() throws Exception {
    in_action.chapter10.EyeImages.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_EyeInlineImage() throws Exception {
    in_action.chapter10.EyeInlineImage.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_EyeLogo() throws Exception {
    in_action.chapter10.EyeLogo.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_FoobarCity() throws Exception {
    in_action.chapter10.FoobarCity.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_GraphicsStateStack() throws Exception {
    in_action.chapter10.GraphicsStateStack.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_InvisibleRectangles() throws Exception {
    in_action.chapter10.InvisibleRectangles.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_LineCharacteristics() throws Exception {
    in_action.chapter10.LineCharacteristics.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_PdfPTableCellEvents() throws Exception {
    in_action.chapter10.PdfPTableCellEvents.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_PdfPTableEvents() throws Exception {
    in_action.chapter10.PdfPTableEvents.main(ARGS);
  }

  @Test
  public void test_in_action_chapter10_PdfPTableFloatingBoxes() throws Exception {
    in_action.chapter10.PdfPTableFloatingBoxes.main(ARGS);
  }

  @Test
  public void test_in_action_chapter11_ClippingPath() throws Exception {
    in_action.chapter11.ClippingPath.main(ARGS);
  }

  @Test
  public void test_in_action_chapter11_ColoredParagraphs() throws Exception {
    in_action.chapter11.ColoredParagraphs.main(ARGS);
  }

  @Test
  public void test_in_action_chapter11_DeviceColor() throws Exception {
    in_action.chapter11.DeviceColor.main(ARGS);
  }

  @Test
  public void test_in_action_chapter11_FoobarCityStreets() throws Exception {
    in_action.chapter11.FoobarCityStreets.main(ARGS);
  }

  @Test
  public void test_in_action_chapter11_Patterns() throws Exception {
    in_action.chapter11.Patterns.main(ARGS);
  }

  @Test
  public void test_in_action_chapter11_SeparationColor() throws Exception {
    in_action.chapter11.SeparationColor.main(ARGS);
  }

  @Test
  public void test_in_action_chapter11_ShadingPatterns() throws Exception {
    in_action.chapter11.ShadingPatterns.main(ARGS);
  }

  @Test
  public void test_in_action_chapter11_TemplateClip() throws Exception {
    in_action.chapter11.TemplateClip.main(ARGS);
  }

  @Test
  public void test_in_action_chapter11_TextMethods() throws Exception {
    in_action.chapter11.TextMethods.main(ARGS);
  }

  @Test
  public void test_in_action_chapter11_TextOperators() throws Exception {
    in_action.chapter11.TextOperators.main(ARGS);
  }

  @Test
  public void test_in_action_chapter11_Transparency1() throws Exception {
    in_action.chapter11.Transparency1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter11_Transparency2() throws Exception {
    in_action.chapter11.Transparency2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter11_Transparency3() throws Exception {
    in_action.chapter11.Transparency3.main(ARGS);
  }

  @Test
  public void test_in_action_chapter12_FoobarCharts() throws Exception {
    in_action.chapter12.FoobarCharts.main(ARGS);
  }

  @Test
  public void test_in_action_chapter12_HindiExample() throws Exception {
    in_action.chapter12.HindiExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapter12_JapaneseExample1() throws Exception {
    in_action.chapter12.JapaneseExample1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter12_JapaneseExample2() throws Exception {
    in_action.chapter12.JapaneseExample2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter12_LayerMembershipExample() throws Exception {
    in_action.chapter12.LayerMembershipExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapter12_OptionalContentActionExample() throws Exception {
    in_action.chapter12.OptionalContentActionExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapter12_OptionalContentExample() throws Exception {
    in_action.chapter12.OptionalContentExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapter12_OptionalXObjectExample() throws Exception {
    in_action.chapter12.OptionalXObjectExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapter12_PeekABoo() throws Exception {
    in_action.chapter12.PeekABoo.main(ARGS);
  }

  @Test
  public void test_in_action_chapter12_SunTutorialExample() throws Exception {
    in_action.chapter12.SunTutorialExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapter12_SunTutorialExampleWithText() throws Exception {
    in_action.chapter12.SunTutorialExampleWithText.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_CourseCatalogBookmarked() throws Exception {
    in_action.chapter13.CourseCatalogBookmarked.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_DocumentLevelJavaScript() throws Exception {
    in_action.chapter13.DocumentLevelJavaScript.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_EventTriggeredActions() throws Exception {
    in_action.chapter13.EventTriggeredActions.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_ExplicitDestinations() throws Exception {
    in_action.chapter13.ExplicitDestinations.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_GotoActions() throws Exception {
    in_action.chapter13.GotoActions.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_HelloWorldCopyBookmarks() throws Exception {
    in_action.chapter13.HelloWorldCopyBookmarks.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_HelloWorldManipulateBookmarks() throws Exception {
    in_action.chapter13.HelloWorldManipulateBookmarks.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_LaunchAction() throws Exception {
    in_action.chapter13.LaunchAction.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_NamedActions() throws Exception {
    in_action.chapter13.NamedActions.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_OutlineActions() throws Exception {
    in_action.chapter13.OutlineActions.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_PageLabels() throws Exception {
    in_action.chapter13.PageLabels.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_PhotoThumbs() throws Exception {
    in_action.chapter13.PhotoThumbs.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_SlideShow() throws Exception {
    in_action.chapter13.SlideShow.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_ThumbImage() throws Exception {
    in_action.chapter13.ThumbImage.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_VPExamples() throws Exception {
    in_action.chapter13.VPExamples.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_VPPageLayout() throws Exception {
    in_action.chapter13.VPPageLayout.main(ARGS);
  }

  @Test
  public void test_in_action_chapter13_VPPageModeAndLayout() throws Exception {
    in_action.chapter13.VPPageModeAndLayout.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_ChapterEvents() throws Exception {
    in_action.chapter14.ChapterEvents.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_CourseCatalogEvents() throws Exception {
    in_action.chapter14.CourseCatalogEvents.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_EmptyPages() throws Exception {
    in_action.chapter14.EmptyPages.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_HeaderFooterExample() throws Exception {
    in_action.chapter14.HeaderFooterExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_HtmlParseExample() throws Exception {
    in_action.chapter14.HtmlParseExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_PageBoundaries() throws Exception {
    in_action.chapter14.PageBoundaries.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_PageXofY() throws Exception {
    in_action.chapter14.PageXofY.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_ParagraphOutlines() throws Exception {
    in_action.chapter14.ParagraphOutlines.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_ParsingHtml() throws Exception {
    in_action.chapter14.ParsingHtml.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_ParsingHtmlSnippets() throws Exception {
    in_action.chapter14.ParsingHtmlSnippets.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_ReorderPages() throws Exception {
    in_action.chapter14.ReorderPages.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_RomeoJuliet() throws Exception {
    in_action.chapter14.RomeoJuliet.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_SimpleLetter() throws Exception {
    in_action.chapter14.SimpleLetter.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_SimpleLetters() throws Exception {
    in_action.chapter14.SimpleLetters.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_SlideShow() throws Exception {
    in_action.chapter14.SlideShow.main(ARGS);
  }

  @Test
  public void test_in_action_chapter14_WatermarkExample() throws Exception {
    in_action.chapter14.WatermarkExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapter15_AnnotatedChunks() throws Exception {
    in_action.chapter15.AnnotatedChunks.main(ARGS);
  }

  @Test
  public void test_in_action_chapter15_AnnotatedImages() throws Exception {
    in_action.chapter15.AnnotatedImages.main(ARGS);
  }

  @Test
  public void test_in_action_chapter15_Annotations() throws Exception {
    in_action.chapter15.Annotations.main(ARGS);
  }

  @Test
  public void test_in_action_chapter15_Buttons() throws Exception {
    in_action.chapter15.Buttons.main(ARGS);
  }

  @Test
  public void test_in_action_chapter15_Buttons2() throws Exception {
    in_action.chapter15.Buttons2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter15_Calculator() throws Exception {
    in_action.chapter15.Calculator.main(ARGS);
  }

  @Test
  public void test_in_action_chapter15_ChoiceFields() throws Exception {
    in_action.chapter15.ChoiceFields.main(ARGS);
  }

  @Test
  public void test_in_action_chapter15_FieldActions() throws Exception {
    in_action.chapter15.FieldActions.main(ARGS);
  }

  @Test
  public void test_in_action_chapter15_SenderReceiver() throws Exception {
    in_action.chapter15.SenderReceiver.main(ARGS);
  }

  @Test
  public void test_in_action_chapter15_SimpleAnnotations() throws Exception {
    in_action.chapter15.SimpleAnnotations.main(ARGS);
  }

  @Test
  public void test_in_action_chapter15_TextAnnotations() throws Exception {
    in_action.chapter15.TextAnnotations.main(ARGS);
  }

  @Test
  public void test_in_action_chapter15_TextFields() throws Exception {
    in_action.chapter15.TextFields.main(ARGS);
  }

  @Test
  public void test_in_action_chapter16_FillAcroForm1() throws Exception {
    in_action.chapter16.FillAcroForm1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter16_FillAcroForm2() throws Exception {
    in_action.chapter16.FillAcroForm2.main(ARGS);
  }

  @Test
  public void test_in_action_chapter16_FillAcroForm3() throws Exception {
    in_action.chapter16.FillAcroForm3.main(ARGS);
  }

  @Test
  public void test_in_action_chapter16_RegisterForm1() throws Exception {
    in_action.chapter16.RegisterForm1.main(ARGS);
  }

  @Test
  public void test_in_action_chapter16_SignedPdf() throws Exception {
    in_action.chapter16.SignedPdf.main(ARGS);
  }

  @Test
  public void test_in_action_chapter16_SignedSignatureField() throws Exception {
    in_action.chapter16.SignedSignatureField.main(ARGS);
  }

  @Test
  public void test_in_action_chapter16_UnsignedSignatureField() throws Exception {
    in_action.chapter16.UnsignedSignatureField.main(ARGS);
  }

  @Test
  public void test_in_action_chapter17_FoobarLearningAgreement() throws Exception {
    in_action.chapter17.FoobarLearningAgreement.main(ARGS);
  }

  @Test
  public void test_in_action_chapter18_ChangeURL() throws Exception {
    in_action.chapter18.ChangeURL.main(ARGS);
  }

  @Test
  public void test_in_action_chapter18_ClimbTheTree() throws Exception {
    in_action.chapter18.ClimbTheTree.main(ARGS);
  }

  @Test
  public void test_in_action_chapter18_HelloWorld() throws Exception {
    in_action.chapter18.HelloWorld.main(ARGS);
  }

  @Test
  public void test_in_action_chapter18_HelloWorldReverse() throws Exception {
    in_action.chapter18.HelloWorldReverse.main(ARGS);
  }

  @Test
  public void test_in_action_chapter18_HelloWorldStream() throws Exception {
    in_action.chapter18.HelloWorldStream.main(ARGS);
  }

  @Test
  public void test_in_action_chapter18_HelloWorldStreamHack() throws Exception {
    in_action.chapter18.HelloWorldStreamHack.main(ARGS);
  }

  @Test
  public void test_in_action_chapter18_SilentPrinting() throws Exception {
    in_action.chapter18.SilentPrinting.main(ARGS);
  }

  @Test
  public void test_in_action_chapterF_HelloWorldAddMetadata() throws Exception {
    in_action.chapterF.HelloWorldAddMetadata.main(ARGS);
  }

  @Test
  public void test_in_action_chapterF_HelloWorldPdfX() throws Exception {
    in_action.chapterF.HelloWorldPdfX.main(ARGS);
  }

  @Test
  public void test_in_action_chapterF_HelloWorldReadMetadata() throws Exception {
    in_action.chapterF.HelloWorldReadMetadata.main(ARGS);
  }

  @Test
  public void test_in_action_chapterF_HelloWorldXmpMetadata() throws Exception {
    in_action.chapterF.HelloWorldXmpMetadata.main(ARGS);
  }

  @Test
  public void test_in_action_chapterF_HelloWorldXmpMetadata2() throws Exception {
    in_action.chapterF.HelloWorldXmpMetadata2.main(ARGS);
  }

  @Test
  public void test_in_action_chapterF_MarkedContent() throws Exception {
    in_action.chapterF.MarkedContent.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_CellImageBackground() throws Exception {
    in_action.chapterX.CellImageBackground.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_ColumnTextExamples() throws Exception {
    in_action.chapterX.ColumnTextExamples.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_EmbedFontPostFacto() throws Exception {
    in_action.chapterX.EmbedFontPostFacto.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_ExtractPageLabels() throws Exception {
    in_action.chapterX.ExtractPageLabels.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_FoobarTranscriptMerge() throws Exception {
    in_action.chapterX.FoobarTranscriptMerge.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_GisExample() throws Exception {
    in_action.chapterX.GisExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_HelloWorldAddButton() throws Exception {
    in_action.chapterX.HelloWorldAddButton.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_HelloWorldEncryptedAES() throws Exception {
    in_action.chapterX.HelloWorldEncryptedAES.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_HelloWorldPackage() throws Exception {
    in_action.chapterX.HelloWorldPackage.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_HelloWorldUnicode() throws Exception {
    in_action.chapterX.HelloWorldUnicode.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_Kalligraphy() throws Exception {
    in_action.chapterX.Kalligraphy.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_NonBreakingSpace() throws Exception {
    in_action.chapterX.NonBreakingSpace.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_NonHyphenatingHyphen() throws Exception {
    in_action.chapterX.NonHyphenatingHyphen.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_NormalDistribution() throws Exception {
    in_action.chapterX.NormalDistribution.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_ReadOutLoud() throws Exception {
    in_action.chapterX.ReadOutLoud.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_ReplacePagesHack() throws Exception {
    in_action.chapterX.ReplacePagesHack.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_ReplacePagesHack2() throws Exception {
    in_action.chapterX.ReplacePagesHack2.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_RotatePages() throws Exception {
    in_action.chapterX.RotatePages.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_TablesInColumns() throws Exception {
    in_action.chapterX.TablesInColumns.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_TooltipExample() throws Exception {
    in_action.chapterX.TooltipExample.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_TooltipExample2() throws Exception {
    in_action.chapterX.TooltipExample2.main(ARGS);
  }

  @Test
  public void test_in_action_chapterX_TooltipExample3() throws Exception {
    in_action.chapterX.TooltipExample3.main(ARGS);
  }

  @Test
  public void test_questions_compression_CompressionLevelsEmbeddedFiles() throws Exception {
    questions.compression.CompressionLevelsEmbeddedFiles.main(ARGS);
  }

  @Test
  public void test_questions_compression_CompressionLevelsFonts() throws Exception {
    questions.compression.CompressionLevelsFonts.main(ARGS);
  }

  @Test
  public void test_questions_compression_CompressionLevelsImage() throws Exception {
    questions.compression.CompressionLevelsImage.main(ARGS);
  }

  @Test
  public void test_questions_compression_CompressionLevelsWriter() throws Exception {
    questions.compression.CompressionLevelsWriter.main(ARGS);
  }

  @Test
  public void test_questions_compression_ZipPdfFiles() throws Exception {
    questions.compression.ZipPdfFiles.main(ARGS);
  }

  @Test
  public void test_questions_directcontent_InterpretOCR() throws Exception {
    questions.directcontent.InterpretOCR.main(ARGS);
  }

  @Test
  public void test_questions_directcontent_Logo() throws Exception {
    questions.directcontent.Logo.main(ARGS);
  }

  @Test
  public void test_questions_encryption_HelloWorldFullyEncrypted() throws Exception {
    questions.encryption.HelloWorldFullyEncrypted.main(ARGS);
  }

  @Test
  public void test_questions_encryption_HelloWorldMetadataNotEncrypted() throws Exception {
    questions.encryption.HelloWorldMetadataNotEncrypted.main(ARGS);
  }

  @Test
  public void test_questions_fonts_EncodingFont() throws Exception {
    questions.fonts.EncodingFont.main(ARGS);
  }

  @Test
  public void test_questions_forms_AddActionToField() throws Exception {
    questions.forms.AddActionToField.main(ARGS);
  }

  @Test
  public void test_questions_forms_AddFieldToExistingForm() throws Exception {
    questions.forms.AddFieldToExistingForm.main(ARGS);
  }

  @Test
  public void test_questions_forms_ChangeTextFieldAlignment() throws Exception {
    questions.forms.ChangeTextFieldAlignment.main(ARGS);
  }

  @Test
  public void test_questions_forms_FillDynamicXfa() throws Exception {
    questions.forms.FillDynamicXfa.main(ARGS);
  }

  @Test
  public void test_questions_forms_FillDynamicXfa2() throws Exception {
    questions.forms.FillDynamicXfa2.main(ARGS);
  }

  @Test
  public void test_questions_forms_FillEnabledForm() throws Exception {
    questions.forms.FillEnabledForm.main(ARGS);
  }

  @Test
  public void test_questions_forms_FillEnabledFormBreakEnabling() throws Exception {
    questions.forms.FillEnabledFormBreakEnabling.main(ARGS);
  }

  @Test
  public void test_questions_forms_FillEnabledFormRemoveEnabling() throws Exception {
    questions.forms.FillEnabledFormRemoveEnabling.main(ARGS);
  }

  @Test
  public void test_questions_forms_FormWithTooltips() throws Exception {
    questions.forms.FormWithTooltips.main(ARGS);
  }

  @Test
  public void test_questions_forms_GetTextFields() throws Exception {
    questions.forms.GetTextFields.main(ARGS);
  }

  @Test
  public void test_questions_forms_KidsOnDifferentPages() throws Exception {
    questions.forms.KidsOnDifferentPages.main(ARGS);
  }

  @Test
  public void test_questions_forms_MultipleChoice() throws Exception {
    questions.forms.MultipleChoice.main(ARGS);
  }

  @Test
  public void test_questions_forms_MultipleChoice2() throws Exception {
    questions.forms.MultipleChoice2.main(ARGS);
  }

  @Test
  public void test_questions_forms_RadioButtonsOnDifferentPages() throws Exception {
    questions.forms.RadioButtonsOnDifferentPages.main(ARGS);
  }

  @Test
  public void test_questions_forms_ReadXfa() throws Exception {
    questions.forms.ReadXfa.main(ARGS);
  }

  @Test
  public void test_questions_forms_ReadXfa2() throws Exception {
    questions.forms.ReadXfa2.main(ARGS);
  }

  @Test
  public void test_questions_forms_RemoveXfa() throws Exception {
    questions.forms.RemoveXfa.main(ARGS);
  }

  @Test
  public void test_questions_graphics2D_ArabicText() throws Exception {
    questions.graphics2D.ArabicText.main(ARGS);
  }

  @Test
  public void test_questions_graphics2D_SplitCanvas() throws Exception {
    questions.graphics2D.SplitCanvas.main(ARGS);
  }

  @Test
  public void test_questions_graphics2D_SplitCanvasDifficult() throws Exception {
    questions.graphics2D.SplitCanvasDifficult.main(ARGS);
  }

  @Test
  public void test_questions_graphics2D_SplitCanvasEasy() throws Exception {
    questions.graphics2D.SplitCanvasEasy.main(ARGS);
  }

  @Test
  public void test_questions_graphics2D_SwingForceArialUni() throws Exception {
    questions.graphics2D.SwingForceArialUni.main(ARGS);
  }

  @Test
  public void test_questions_images_CustomizedTitleBar() throws Exception {
    questions.images.CustomizedTitleBar.main(ARGS);
  }

  @Test
  public void test_questions_images_MakingImageTransparent() throws Exception {
    questions.images.MakingImageTransparent.main(ARGS);
  }

  @Test
  public void test_questions_images_OverlappingImages() throws Exception {
    questions.images.OverlappingImages.main(ARGS);
  }

  @Test
  public void test_questions_images_PostCard() throws Exception {
    questions.images.PostCard.main(ARGS);
  }

  @Test
  public void test_questions_images_PostCardExtra() throws Exception {
    questions.images.PostCardExtra.main(ARGS);
  }

  @Test
  public void test_questions_images_ResizeImage() throws Exception {
    questions.images.ResizeImage.main(ARGS);
  }

  @Test
  public void test_questions_images_TransparentEllipse1() throws Exception {
    questions.images.TransparentEllipse1.main(ARGS);
  }

  @Test
  public void test_questions_images_TransparentEllipse2() throws Exception {
    questions.images.TransparentEllipse2.main(ARGS);
  }

  @Test
  public void test_questions_images_TransparentPng() throws Exception {
    questions.images.TransparentPng.main(ARGS);
  }

  @Test
  public void test_questions_importpages_ConcatenateMakeTOC() throws Exception {
    questions.importpages.ConcatenateMakeTOC.main(ARGS);
  }

  @Test
  public void test_questions_importpages_ConcatenateWithTOC() throws Exception {
    questions.importpages.ConcatenateWithTOC.main(ARGS);
  }

  @Test
  public void test_questions_importpages_ConcatenateWithTOC2() throws Exception {
    questions.importpages.ConcatenateWithTOC2.main(ARGS);
  }

  @Test
  public void test_questions_importpages_ExportArea() throws Exception {
    questions.importpages.ExportArea.main(ARGS);
  }

  @Test
  public void test_questions_importpages_HelloWorldImportedPages() throws Exception {
    questions.importpages.HelloWorldImportedPages.main(ARGS);
  }

  @Test
  public void test_questions_importpages_NameCard() throws Exception {
    questions.importpages.NameCard.main(ARGS);
  }

  @Test
  public void test_questions_importpages_NameCards() throws Exception {
    questions.importpages.NameCards.main(ARGS);
  }

  @Test
  public void test_questions_javascript_AddJavaScriptToForm() throws Exception {
    questions.javascript.AddJavaScriptToForm.main(ARGS);
  }

  @Test
  public void test_questions_javascript_TriggerMenuButtons() throws Exception {
    questions.javascript.TriggerMenuButtons.main(ARGS);
  }

  @Test
  public void test_questions_javascript_VersionChecker() throws Exception {
    questions.javascript.VersionChecker.main(ARGS);
  }

  @Test
  public void test_questions_markedcontent_ObjectData() throws Exception {
    questions.markedcontent.ObjectData.main(ARGS);
  }

  @Test
  public void test_questions_metadata_ReplaceXMP() throws Exception {
    questions.metadata.ReplaceXMP.main(ARGS);
  }

  @Test
  public void test_questions_metadata_UpdateModDate() throws Exception {
    questions.metadata.UpdateModDate.main(ARGS);
  }

  @Test
  public void test_questions_objects_ChaptersAndMemory() throws Exception {
    questions.objects.ChaptersAndMemory.main(ARGS);
  }

  @Test
  public void test_questions_objects_DifferentLeadings() throws Exception {
    questions.objects.DifferentLeadings.main(ARGS);
  }

  @Test
  public void test_questions_objects_MemoryTests() throws Exception {
    questions.objects.MemoryTests.main(ARGS);
  }

  @Test
  public void test_questions_objects_NestedList() throws Exception {
    questions.objects.NestedList.main(ARGS);
  }

  @Test
  public void test_questions_objects_NestingLists() throws Exception {
    questions.objects.NestingLists.main(ARGS);
  }

  @Test
  public void test_questions_objects_NewPageColumns() throws Exception {
    questions.objects.NewPageColumns.main(ARGS);
  }

  @Test
  public void test_questions_ocg_AddOptionalWatermark() throws Exception {
    questions.ocg.AddOptionalWatermark.main(ARGS);
  }

  @Test
  public void test_questions_ocg_LockedLayers() throws Exception {
    questions.ocg.LockedLayers.main(ARGS);
  }

  @Test
  public void test_questions_ocg_StatusBars1() throws Exception {
    questions.ocg.StatusBars1.main(ARGS);
  }

  @Test
  public void test_questions_ocg_StatusBars2() throws Exception {
    questions.ocg.StatusBars2.main(ARGS);
  }

  @Test
  public void test_questions_separators_DottedGlue() throws Exception {
    questions.separators.DottedGlue.main(ARGS);
  }

  @Test
  public void test_questions_separators_LineSeparator1() throws Exception {
    questions.separators.LineSeparator1.main(ARGS);
  }

  @Test
  public void test_questions_separators_LineSeparator2() throws Exception {
    questions.separators.LineSeparator2.main(ARGS);
  }

  @Test
  public void test_questions_separators_LineSeparator3() throws Exception {
    questions.separators.LineSeparator3.main(ARGS);
  }

  @Test
  public void test_questions_separators_PascalsTriangle() throws Exception {
    questions.separators.PascalsTriangle.main(ARGS);
  }

  @Test
  public void test_questions_separators_PositionedMarks() throws Exception {
    questions.separators.PositionedMarks.main(ARGS);
  }

  @Test
  public void test_questions_separators_SeparatedWords1() throws Exception {
    questions.separators.SeparatedWords1.main(ARGS);
  }

  @Test
  public void test_questions_separators_SeparatedWords2() throws Exception {
    questions.separators.SeparatedWords2.main(ARGS);
  }

  @Test
  public void test_questions_separators_StarSeparators() throws Exception {
    questions.separators.StarSeparators.main(ARGS);
  }

  @Test
  public void test_questions_separators_TabbedWords1() throws Exception {
    questions.separators.TabbedWords1.main(ARGS);
  }

  @Test
  public void test_questions_separators_TabbedWords2() throws Exception {
    questions.separators.TabbedWords2.main(ARGS);
  }

  @Test
  public void test_questions_separators_TOCExample() throws Exception {
    questions.separators.TOCExample.main(ARGS);
  }

  @Test
  public void test_questions_stamppages_AddCropbox() throws Exception {
    questions.stamppages.AddCropbox.main(ARGS);
  }

  @Test
  public void test_questions_stamppages_BookmarksToTOC1() throws Exception {
    questions.stamppages.BookmarksToTOC1.main(ARGS);
  }

  @Test
  public void test_questions_stamppages_BookmarksToTOC2() throws Exception {
    questions.stamppages.BookmarksToTOC2.main(ARGS);
  }

  @Test
  public void test_questions_stamppages_CertificationSig() throws Exception {
    questions.stamppages.CertificationSig.main(ARGS);
  }

  @Test
  public void test_questions_stamppages_ChangeViewerPreferences() throws Exception {
    questions.stamppages.ChangeViewerPreferences.main(ARGS);
  }

  @Test
  public void test_questions_stamppages_IncreaseMediabox() throws Exception {
    questions.stamppages.IncreaseMediabox.main(ARGS);
  }

  @Test
  public void test_questions_stamppages_PageXofYRightAligned() throws Exception {
    questions.stamppages.PageXofYRightAligned.main(ARGS);
  }

  @Test
  public void test_questions_stamppages_RemoveAttachmentAnnotations() throws Exception {
    questions.stamppages.RemoveAttachmentAnnotations.main(ARGS);
  }

  @Test
  public void test_questions_tables_AddTableAsHeaderFooter() throws Exception {
    questions.tables.AddTableAsHeaderFooter.main(ARGS);
  }

  @Test
  public void test_questions_tables_AutomaticExtensionOfTables() throws Exception {
    questions.tables.AutomaticExtensionOfTables.main(ARGS);
  }

  @Test
  public void test_questions_tables_RotateCell() throws Exception {
    questions.tables.RotateCell.main(ARGS);
  }

  @Test
  public void test_questions_tables_RotatedCells() throws Exception {
    questions.tables.RotatedCells.main(ARGS);
  }

  @Test
  public void test_questions_tables_TableAndHTMLWorker() throws Exception {
    questions.tables.TableAndHTMLWorker.main(ARGS);
  }

  @Test
  public void test_questions_tables_TableColumns() throws Exception {
    questions.tables.TableColumns.main(ARGS);
  }

  @Test
  public void test_questions_tables_TableHeaderAlternateBackground() throws Exception {
    questions.tables.TableHeaderAlternateBackground.main(ARGS);
  }

  @Test
  public void test_questions_tables_TablesWriteSelected() throws Exception {
    questions.tables.TablesWriteSelected.main(ARGS);
  }

  @Test
  public void test_rtf_intro_RTFHelloWorld1() throws Exception {
    rtf.intro.RTFHelloWorld1.main(ARGS);
  }

  @Test
  public void test_rtf_shapes_RTFShapeTextWrap() throws Exception {
    rtf.shapes.RTFShapeTextWrap.main(ARGS);
  }

}
