#!/bin/sh

FILENAME=$2

cat <<EOF > $FILENAME
package examples;

import org.junit.Test;

public class TestAll {

  private static final String ARGS[] = {};

EOF

# test list

# FIXME FIXME FIXME
#
#  in_action.chapter12.FoobarCityBatik and
#  classroom.filmfestival_c.Movies25 shouldn't be disabled
# problem in classloading with xerces
#
for i in `grep "java " $1/all.sh | sed -e "s/^.*\" //g" -e "s/\//./g" \
      -e "/classroom.filmfestival_c.Movies25/d" \
      -e "/in_action.chapter12.FoobarCityBatik/d" \
      | sort| uniq `; do

   j=`echo $i |sed -e "s/\./_/g"`
   echo "  @Test" >> $FILENAME;
   echo "  public void test_$j() throws Exception {" >> $FILENAME;
   echo "    $i.main(ARGS);" >> $FILENAME;
   echo "  }" >> $FILENAME;
   echo "" >> $FILENAME;

done

echo "}" >> $FILENAME
